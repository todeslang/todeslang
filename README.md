# Todeslang

## Install

```
sudo apt install libssl-dev libcrypto++-dev

cd .../src/libthel
make clean
make
sudo make install

cd .../src/libtodes
make clean
make
sudo make install

cd .../src/todes
make clean
make
sudo make install

cd .../src/todesweb
make clean
make
sudo make install
```

## Acknowledgements

* https://github.com/cesanta/mongoose

* https://curl.haxx.se/docs/caextract.html

* https://github.com/kazuho/picojson
