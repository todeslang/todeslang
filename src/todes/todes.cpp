#include <fstream>

#include <thel.h>
#include <todes.h>

#include "todesfilesystem.h"


using namespace std;


class PrimitiveGetProgramArgument: public todes::Primitive {
public:
	int argc;
	char ** argv;
public:
        void run ();
};


void PrimitiveGetProgramArgument::run ()
{
	auto main_value = get_parameter ();
	auto main_number = dynamic_pointer_cast <todes::NumberValue> (main_value);
	if (main_number) {
		auto index = main_number->value;
		if (index < argc) {
			set_return_value (make_shared <todes::StringValue> (string {argv [index]}));
		}
	}
}


int main (int argc, char ** argv)
{
	string workspace = 1 < argc? string {argv [1]}: string {};
	string additional_glob = 2 < argc? string {argv [2]}: string {};

	vector <string> file_names = todes_filesystem::get_file_names (workspace, additional_glob);

	string super_cjson;

	for (auto file_name: file_names) {
		string in_string;
		ifstream in_file {file_name};
		if (in_file) {
			for (; ; ) {
				if (in_file.eof ()) {
					break;
				}
				string buffer;
				getline (in_file, buffer);
				in_string += buffer + string {"\n"};
			}
			string cjson = thel::parse (in_string, file_name);
			super_cjson += cjson + string {"\n"};
		} else {
			cerr << "Can not open a file: " << file_name << endl;
		}
	}
	
	todes::NodeProgram node_program;
	node_program.read (super_cjson);

	todes::Program program;
	program.compile (node_program);

	todes::StaticEnvironment senv;
	senv.program = program;
	senv.initialize ();

	auto primitive_get_program_argument = make_shared <PrimitiveGetProgramArgument> ();
	primitive_get_program_argument->argc = argc;
	primitive_get_program_argument->argv = argv;
	senv.primitives.insert (pair {string {"getprogramargument"}, primitive_get_program_argument});

	senv.run ();
	
	return 0;
}


