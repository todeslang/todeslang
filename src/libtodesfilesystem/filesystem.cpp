#include <glob.h>
#include <filesystem>
#include <iostream>
#include <algorithm>

#include "todesfilesystem.h"


using namespace std;


static int do_nothing (const char *epath, int errorno)
{
	return 0;
}


static vector <string> glob (string pattern, int flags)
{
	glob_t glob_struct;

	int glob_result = glob (
		pattern.c_str (),
		flags,
		do_nothing,
		& glob_struct
	);

	vector <string> file_names;

	if (glob_result == 0) {
		for (size_t cn = 0; cn < glob_struct.gl_pathc; cn ++) {
			file_names.push_back (string {glob_struct.gl_pathv [cn]});
		}
	}

	globfree (& glob_struct);
	
	return file_names;
}


static bool by_pure_file_name (string left, string right)
{
	return filesystem::path {left}.filename ().string ()
		< filesystem::path {right}.filename ().string ();
}


vector <string> todes_filesystem::get_file_names (string workspace, string additional_glob)
{
	vector <string> file_names;

	if (! workspace.empty ()) {
		auto file_names_1 = glob (string {"/usr/share/todes/"} + workspace + string {"/*/*.tode"}, 0);
		file_names.insert (file_names.end (), file_names_1.begin (), file_names_1.end ());

		auto file_names_2 = glob (string {"/usr/local/share/todes/"} + workspace + string {"/*/*.tode"}, 0);
		file_names.insert (file_names.end (), file_names_2.begin (), file_names_2.end ());

		auto file_names_3 = glob (string {"~/.todes/share/"} + workspace + string {"/*/*.tode"}, GLOB_TILDE_CHECK);
		file_names.insert (file_names.end (), file_names_3.begin (), file_names_3.end ());
	}

	if (! additional_glob.empty ()) {
		auto file_names_4 = glob (additional_glob, GLOB_BRACE | GLOB_TILDE_CHECK);
		if (file_names_4.empty ()) {
			cerr << "Glob does not hit: " << additional_glob << endl;
		} else {
			file_names.insert (file_names.end (), file_names_4.begin (), file_names_4.end ());
		}
	}

	stable_sort (file_names.begin (), file_names.end (), by_pure_file_name);

	return file_names;
}


