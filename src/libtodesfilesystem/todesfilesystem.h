#ifndef TODESFILESYSTEM_H
#define TODESFILESYSTEM_H


namespace todes_filesystem {


using namespace std;


vector <string> get_file_names (string workspace, string additional_glob);


}; /* namespace todes_filesystem { */


#endif /* #ifndef TODESFILESYSTEM_H */

