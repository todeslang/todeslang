#include <fstream>
#include <ctime>
#include <sstream>

#include <thel.h>
#include <todes.h>
#include <todesfilesystem.h>

#include "todesweb.h"


using namespace std;


/* Global */


struct mg_mgr todesweb::mongoose_manager;
deque <todesweb::Task> todesweb::tasks;


/* PrimitiveGetProgramArgument */


class PrimitiveGetProgramArgument: public todes::Primitive {
public:
	int argc;
	char ** argv;
public:
        void run ();
};


void PrimitiveGetProgramArgument::run ()
{
	auto main_value = get_parameter ();
	auto main_number = dynamic_pointer_cast <todes::NumberValue> (main_value);
	if (main_number) {
		auto index = main_number->value;
		if (index < argc) {
			set_return_value (make_shared <todes::StringValue> (string {argv [index]}));
		}
	}
}


/* Dump */


string dump_to_string (vector <map <string, shared_ptr <todes::Value>>> dump)
{
	stringstream out_stream;
	for (size_t cn = 0; cn < dump.size (); cn ++) {
		out_stream << cn << " ";
		auto parameters = dump.at (cn);
		if (parameters.find (string {"action"}) != parameters.end ()) {
			out_stream << "action: " << parameters.at (string {"action"})->format () << "; ";
		}
		if (parameters.find (string {"state"}) != parameters.end ()) {
			out_stream << "state: " << parameters.at (string {"state"})->format () << "; ";
		}
		if (parameters.find (string {"main"}) != parameters.end ()) {
			out_stream << "main: " << parameters.at (string {"main"})->format () << "; ";
		}
		out_stream << endl;
	}
	return out_stream.str ();
}


/* Main */


int main (int argc, char ** argv)
{
	string workspace = 1 < argc? string {argv [1]}: string {};
	string additional_glob = 2 < argc? string {argv [2]}: string {};

	vector <string> file_names = todes_filesystem::get_file_names (workspace, additional_glob);

	string super_cjson;

	for (auto file_name: file_names) {
		string in_string;
		ifstream in_file {file_name};
		if (in_file) {
			for (; ; ) {
				if (in_file.eof ()) {
					break;
				}
				string buffer;
				getline (in_file, buffer);
				in_string += buffer + string {"\n"};
			}
			string cjson = thel::parse (in_string, file_name);
			super_cjson += cjson + string {"\n"};
		} else {
			cerr << "Can not open a file: " << file_name << endl;
		}
	}
	
	todes::NodeProgram node_program;
	node_program.read (super_cjson);

	todes::Program program;
	program.compile (node_program);

	todes::StaticEnvironment senv;
	senv.program = program;
	senv.initialize ();

	auto primitive_get_program_argument = make_shared <PrimitiveGetProgramArgument> ();
	primitive_get_program_argument->argc = argc;
	primitive_get_program_argument->argv = argv;
	senv.primitives.insert (pair {string {"getprogramargument"}, primitive_get_program_argument});

	map <string, shared_ptr <todes::Primitive>> web_primitives = todesweb::get_web_primitives ();
	senv.primitives.insert (web_primitives.begin (), web_primitives.end ());

	mg_mgr_init (& todesweb::mongoose_manager);

	senv.run ();

	for (; ; ) {
		mg_mgr_poll (& todesweb::mongoose_manager, 10);

		if (! todesweb::tasks.empty ()) {
			auto task = todesweb::tasks.front ();
			todesweb::tasks.pop_front ();

			vector <map <string, shared_ptr<todes::Value>>> dump_before;
			task.env.dump (dump_before);

			time_t start_time = time (nullptr);
			task.run ();
			time_t end_time = time (nullptr);
			
			vector <map <string, shared_ptr<todes::Value>>> dump_after;
			if (! todesweb::tasks.empty ()) {
				todesweb::tasks.back ().env.dump (dump_after);
			}
			
			time_t consume = end_time - start_time;
			if (10 <= consume) {
				cerr << "Task time: " << consume << endl;
				cerr << "Before:" << endl;
				cerr << dump_to_string (dump_before);
				cerr << "After:" << endl;
				cerr << dump_to_string (dump_after);
			}
		}
	}

	mg_mgr_free (& todesweb::mongoose_manager);
	
	return 0;
}


