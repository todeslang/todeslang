#include <iostream>

#include "todesweb.h"


using namespace std;


static string make_string (struct mg_str in)
{
	return string {in.ptr, in.len};
}


void todesweb::CapsuleWeb::run ()
{
	auto method_value = get_parameter (string {"method"});
	auto method_string_value = dynamic_pointer_cast <todes::StringValue> (method_value);
	string method = method_string_value->value;

	auto main_value = get_parameter ();
	auto main_string_value = dynamic_pointer_cast <todes::StringValue> (main_value);
	string main;
	if (main_string_value) {
		main = main_string_value->value;
	}

	if (method == string {"Web"}) {
		set_return_value (make_shared <todes::BooleanValue> (true));
	} else if (method == string {"get"}) {
		if (main == string {"method"}) {
			set_return_value (make_shared <todes::StringValue> (make_string (http_message->method)));
		} else if (main == string {"uri"}) {
			set_return_value (make_shared <todes::StringValue> (make_string (http_message->uri)));
		} else if (main == string {"query"}) {
			set_return_value (make_shared <todes::StringValue> (make_string (http_message->query)));
		} else if (main == string {"proto"}) {
			set_return_value (make_shared <todes::StringValue> (make_string (http_message->proto)));
		} else if (main == string {"headers"}) {
			/* WIP */
		} else if (main == string {"body"}) {
			set_return_value (make_shared <todes::StringValue> (make_string (http_message->body)));
		} else if (main == string {"message"}) {
			set_return_value (make_shared <todes::StringValue> (make_string (http_message->message)));
		}
	} else if (method == string {"reply"}) {
		int status_code = 200;
		auto status_value = get_parameter ("status");
		auto status_number_value = dynamic_pointer_cast <todes::NumberValue> (status_value);
		if (status_number_value) {
			status_code = status_number_value->value;
		}
		
		string headers {"Content-Type: text/html\r\n"};
		auto headers_value = get_parameter ("headers");
		auto headers_string_value = dynamic_pointer_cast <todes::StringValue> (headers_value);
		if (headers_string_value) {
			headers = headers_string_value->value;
		}
		
		mg_http_reply (connection, status_code, headers.c_str (), "%s", main.c_str ());
	} else if (method == string {"servefile"}) {
		string mime;
		auto mime_value = get_parameter ("extramime");
		auto mime_string_value = dynamic_pointer_cast <todes::StringValue> (mime_value);
		if (mime_string_value) {
			mime = mime_string_value->value;
		}
			
		string headers;
		auto headers_value = get_parameter ("headers");
		auto headers_string_value = dynamic_pointer_cast <todes::StringValue> (headers_value);
		if (headers_string_value) {
			headers = headers_string_value->value;
		}

		struct mg_http_serve_opts opts;
		opts.root_dir = nullptr;
		opts.ssi_pattern = nullptr;
		opts.extra_headers = headers.empty ()? nullptr: headers.c_str ();
		opts.mime_types = mime.empty ()? nullptr: mime.c_str ();
		opts.fs = nullptr;

		mg_http_serve_file (
			connection,
			http_message,
			main.c_str (),
			& opts
		);
	} else if (method == string {"servedir"}) {
		struct mg_http_serve_opts opts {
			.root_dir = main.c_str (),
			.ssi_pattern = nullptr
		};
		mg_http_serve_dir (
			connection,
			http_message,
			& opts
		);
	}
}


