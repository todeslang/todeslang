#ifndef TODESWEB_H
#define TODESWEB_H

#include <deque>

#include <todes.h>
#include "mongoose.h"


namespace todesweb {


using namespace std;


/* Global */


extern struct mg_mgr mongoose_manager;
extern deque <class Task> tasks;


/* Classes */


struct ClientAnything {
	string url;
	todes::DynamicEnvironment env;
};


class CapsuleWeb: public todes::Capsule {
public:
	struct mg_connection *connection;
	struct mg_http_message *http_message;
public:
	void run ();
};


class Task {
public:
	todes::DynamicEnvironment env;
public:
	void run () {
		env.run (make_shared <todes::BooleanValue> (false));
	}
};


/* Functions */


map <string, shared_ptr <todes::Primitive>> get_web_primitives ();
void server_callback (struct mg_connection *c, int ev, void *ev_data, void *fn_data);
void client_callback (struct mg_connection *c, int ev, void *ev_data, void *fn_data);


}; /* namespace todesweb { */


#endif /* #ifndef TODESWEB_H */

