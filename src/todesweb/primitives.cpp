#include <set>
#include <map>
#include <algorithm>
#include <vector>
#include <ctime>
#include <iostream>
#include <cstdlib>

#include "todesweb.h"


using namespace std;


/* Primitives */


class PrimitiveWebServerListen: public todes::Primitive {
public:
        void run ();
};


void PrimitiveWebServerListen::run ()
{
	auto url_value = get_parameter ("url");
	auto url_string_value = dynamic_pointer_cast <todes::StringValue> (url_value);
	string url_string = url_string_value->value;

	auto callback_value = get_parameter ("callback");
	auto callback_continuation_value = dynamic_pointer_cast <todes::ContinuationValue> (callback_value);
	auto env_p = callback_continuation_value->get_dynamic_environment_ptr ();
	auto env_copy = new todes::DynamicEnvironment {* env_p}; /* Never delete. */
	
	mg_http_listen (& todesweb::mongoose_manager, url_string.c_str (), todesweb::server_callback, env_copy);
}


class PrimitiveWebClientConnect: public todes::Primitive {
public:
        void run ();
};


void PrimitiveWebClientConnect::run ()
{
	auto url_value = get_parameter ("url");
	auto url_string_value = dynamic_pointer_cast <todes::StringValue> (url_value);
	string url_string = url_string_value->value;

	auto callback_value = get_parameter ("callback");
	auto callback_continuation_value = dynamic_pointer_cast <todes::ContinuationValue> (callback_value);
	auto env_p = callback_continuation_value->get_dynamic_environment_ptr ();

	auto client_anything = new todesweb::ClientAnything {}; /* Delete in callback. */
	
	client_anything->url = url_string;
	client_anything->env = (* env_p);
	
	mg_http_connect (& todesweb::mongoose_manager, url_string.c_str (), todesweb::client_callback, client_anything);
}


class PrimitiveRegisterTask: public todes::Primitive {
public:
        void run ();
};


void PrimitiveRegisterTask::run ()
{
	auto main_value = get_parameter ("main");
	auto main_continuation_value = dynamic_pointer_cast <todes::ContinuationValue> (main_value);
	auto env_p = main_continuation_value->get_dynamic_environment_ptr ();

	todesweb::Task task;
	task.env = (* env_p);

	todesweb::tasks.push_back (task);
}

class PrimitiveGetTaskSize: public todes::Primitive {
public:
        void run ();
};


void PrimitiveGetTaskSize::run ()
{
	set_return_value (make_shared <todes::NumberValue> (
		todesweb::tasks.size ()
	));
}


map <string, shared_ptr <todes::Primitive>> todesweb::get_web_primitives ()
{
	return map <string, shared_ptr <todes::Primitive>> {
		{string {"webserverlisten"}, make_shared <PrimitiveWebServerListen> ()},
		{string {"webclientconnect"}, make_shared <PrimitiveWebClientConnect> ()},
		{string {"registertask"}, make_shared <PrimitiveRegisterTask> ()},
		{string {"gettasksize"}, make_shared <PrimitiveGetTaskSize> ()},
	};
}


/* Callback */


void todesweb::server_callback (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	if (ev == MG_EV_HTTP_MSG) {
		auto http_message = reinterpret_cast <struct mg_http_message *> (ev_data);
		auto env_p = reinterpret_cast <todes::DynamicEnvironment *> (fn_data);

		auto capsule_web = make_shared <todesweb::CapsuleWeb> ();
		capsule_web->connection = c;
		capsule_web->http_message = http_message;
		
		auto capsule_web_value = make_shared <todes::CapsuleValue> (capsule_web);
		
		env_p->run (capsule_web_value);
	}
}


void todesweb::client_callback (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	todesweb::ClientAnything * client_anything = reinterpret_cast <todesweb::ClientAnything *> (fn_data);

	const char *url = client_anything->url.c_str ();

	if (ev == MG_EV_CONNECT) {
		struct mg_str host = mg_url_host (url);
		if (mg_url_is_ssl (url)) {
			struct mg_tls_opts opts = {
				.ca = "/etc/todesweb/pem/cacert.pem",
				.srvname = host,
			};
			mg_tls_init (c, &opts);
		}
		mg_printf (
			c,
			"GET %s HTTP/1.0\r\nHost: %.*s\r\n\r\n",
			mg_url_uri (url),
			(int) host.len,
			host.ptr
		);
	} else if (ev == MG_EV_HTTP_MSG) {
		auto http_message = reinterpret_cast <struct mg_http_message *> (ev_data);
		todes::DynamicEnvironment env = client_anything->env;

		auto capsule_web = make_shared <todesweb::CapsuleWeb> ();
		capsule_web->connection = c;
		capsule_web->http_message = http_message;
		
		auto capsule_web_value = make_shared <todes::CapsuleValue> (capsule_web);
		
		env.run (capsule_web_value);

		delete client_anything;
		c->is_closing = 1;
	}
}


