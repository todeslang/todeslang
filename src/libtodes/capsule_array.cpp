#include "todes.h"


using namespace std;
using namespace todes;


void CapsuleArray::run ()
{
	auto method_value = get_parameter (string {"method"});
	auto method_string = dynamic_pointer_cast <StringValue> (method_value);
	if (! method_string) {
		return;
	}
	string method = method_string->value;

	auto main_value = get_parameter ();
	auto main_number = dynamic_pointer_cast <NumberValue> (main_value);
	auto main_string = dynamic_pointer_cast <StringValue> (main_value);
	auto at_number = dynamic_pointer_cast <NumberValue> (get_parameter (string {"at"}));
	
	if (method == string {"Array"}) {
		set_return_value (make_shared <BooleanValue> (true));
	} else if (method == string {"size"}) {
		set_return_value (make_shared <NumberValue> (container.size ()));
	} else if (method == string {"at"}) {
		if (main_number) {
			set_return_value (container.at (main_number->value));
		}
	} else if (method == string {"replace"}) {
		if (at_number) {
			auto new_container = container;
			new_container.at (at_number->value) = get_parameter ("by");
			set_return_value (make_shared <CapsuleValue> (
				make_shared <CapsuleArray> (new_container)
			));
		}
	} else if (method == string {"popfront"}) {
		auto new_container = container;
		new_container.pop_front ();
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleArray> (new_container)
		));
	} else if (method == string {"popback"}) {
		auto new_container = container;
		new_container.pop_back ();
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleArray> (new_container)
		));
	} else if (method == string {"tostring"}) {
		string s;
		for (auto value: container) {
			auto number_value = dynamic_pointer_cast <NumberValue> (value);
			if (! number_value) {
				throw (GuestCatchableException {"Array::tostring.", __FILE__, __LINE__});
			}
			s.push_back (static_cast <char> (number_value->value));
		}
		set_return_value (make_shared <StringValue> (s));
	} else if (method == string {"fromstring"}) {
		auto string_main = dynamic_pointer_cast <StringValue> (get_parameter ());
		if (! string_main) {
			throw (GuestCatchableException {"Array::fromstring.", __FILE__, __LINE__});
		}
		vector <shared_ptr <Value>> new_container;
		for (char c: string_main->value) {
			size_t number = static_cast <size_t> (static_cast <unsigned char> (c));
			new_container.push_back (make_shared <NumberValue> (number));
			set_return_value (make_shared <CapsuleValue> (
				make_shared <CapsuleArray> (new_container)
			));
		}
	} else if (method == string {"sort"}) {
		Comparer comparer;
		auto continuation_main = dynamic_pointer_cast <ContinuationValue> (get_parameter ());
		if (continuation_main) {
			shared_ptr <DynamicEnvironment> env_p
				= continuation_main->get_dynamic_environment_ptr ();
			if (env_p) {
				comparer.continuation = env_p;
			}
		}
		auto new_container =container;
		stable_sort (new_container.begin (), new_container.end (), comparer);
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleArray> (new_container)
		));
	} else if (method == string {"has"}) {
		bool hit = false;
		for (auto item: container) {
			if (equal (item, main_value)) {
				hit = true;
				break;
			}
		}
		set_return_value (make_shared <BooleanValue> (hit));
	} else if (method == string {"pushfront"}) {
		auto main_value = get_parameter ();
		auto new_container = container;
		new_container.push_front (main_value);
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleArray> (new_container)
		));
	} else if (method == string {"equal"}) {
		auto main_value = get_parameter ();
		auto main_capsule_value = dynamic_pointer_cast <CapsuleValue> (main_value);
		auto main_array_container = dynamic_pointer_cast <CapsuleArray> (main_capsule_value->value);
		deque <shared_ptr <Value>> main_array = main_array_container->container;
		
		if (container.size () == main_array.size ()) {
			bool equal_bool = true;
			for (size_t cn = 0; cn < container.size (); cn ++) {
				if (! equal (container.at (cn), main_array.at (cn))) {
					equal_bool = false;
					break;
				}
			}
			set_return_value (make_shared <BooleanValue> (equal_bool));
		} else {
			set_return_value (make_shared <BooleanValue> (false));
		}
	} else if (method == string {"startswith"}) {
		auto main_value = get_parameter ();
		auto main_capsule_value = dynamic_pointer_cast <CapsuleValue> (main_value);
		auto main_array_container = dynamic_pointer_cast <CapsuleArray> (main_capsule_value->value);
		deque <shared_ptr <Value>> main_array = main_array_container->container;
		
		if (main_array.size () <= container.size ()) {
			bool equal_bool = true;
			for (size_t cn = 0; cn < main_array.size (); cn ++) {
				if (! equal (container.at (cn), main_array.at (cn))) {
					equal_bool = false;
					break;
				}
			}
			set_return_value (make_shared <BooleanValue> (equal_bool));
		} else {
			set_return_value (make_shared <BooleanValue> (false));
		}
	} else if (method == string {"endswith"}) {
		auto main_value = get_parameter ();
		auto main_capsule_value = dynamic_pointer_cast <CapsuleValue> (main_value);
		auto main_array_container = dynamic_pointer_cast <CapsuleArray> (main_capsule_value->value);
		deque <shared_ptr <Value>> main_array = main_array_container->container;
		
		if (main_array.size () <= container.size ()) {
			size_t offset = container.size () - main_array.size ();
			bool equal_bool = true;
			for (size_t cn = 0; cn < main_array.size (); cn ++) {
				if (! equal (container.at (cn + offset), main_array.at (cn))) {
					equal_bool = false;
					break;
				}
			}
			set_return_value (make_shared <BooleanValue> (equal_bool));
		} else {
			set_return_value (make_shared <BooleanValue> (false));
		}
	} else if (method == string {"slice"}) {
		auto offset_value = get_parameter ("offset");
		size_t offset_number = dynamic_pointer_cast <NumberValue> (offset_value)->value;
		auto size_value = get_parameter ("size");
		size_t size_number = dynamic_pointer_cast <NumberValue> (size_value)->value;

		vector <shared_ptr <Value>> new_container;

		for (size_t cn = 0; cn < size_number; cn ++) {
			new_container.push_back (container.at (cn + offset_number));
		}

		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleArray> (new_container)
		));
	} else if (method == string {"findindexall"}) {
		auto main_value = get_parameter ();
		auto main_capsule_value = dynamic_pointer_cast <CapsuleValue> (main_value);
		auto main_array_container = dynamic_pointer_cast <CapsuleArray> (main_capsule_value->value);
		deque <shared_ptr <Value>> main_array = main_array_container->container;
		
		vector <size_t> founds;
		
		if (main_array.size () <= container.size ())
		for (size_t cn_a = 0; cn_a <= container.size () - main_array.size (); cn_a ++) {
			bool equal_bool = true;
			for (size_t cn_b = 0; cn_b < main_array.size (); cn_b ++) {
				if (! equal (container.at (cn_a + cn_b), main_array.at (cn_b))) {
					equal_bool = false;
					break;
				}
			}
			if (equal_bool) {
				founds.push_back (cn_a);
			}
		}

		deque <shared_ptr <Value>> new_container;

		for (auto found: founds) {
			new_container.push_back (make_shared <NumberValue> (found));
		}

		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleArray> (new_container)
		));
	} else if (method == string {"replaceall"}) {
		auto from_value = get_parameter ("from");
		auto from_capsule_value = dynamic_pointer_cast <CapsuleValue> (from_value);
		auto from_array_container = dynamic_pointer_cast <CapsuleArray> (from_capsule_value->value);
		deque <shared_ptr <Value>> from_array = from_array_container->container;
		
		auto to_value = get_parameter ("to");
		auto to_capsule_value = dynamic_pointer_cast <CapsuleValue> (to_value);
		auto to_array_container = dynamic_pointer_cast <CapsuleArray> (to_capsule_value->value);
		deque <shared_ptr <Value>> to_array = to_array_container->container;
		
		deque <shared_ptr <Value>> new_container;

		for (size_t cn_a = 0; cn_a < container.size (); /* None */) {
			bool equal_bool = false;
			if (cn_a + from_array.size () <= container.size ()) {
				equal_bool = true;
				for (size_t cn_b = 0; cn_b < from_array.size (); cn_b ++) {
					if (! equal (container.at (cn_a + cn_b), from_array.at (cn_b))) {
						equal_bool = false;
					}
				}
			}
			if (equal_bool) {
				for (auto to_value: to_array) {
					new_container.push_back (to_value);
				}
				cn_a += from_array.size ();
			} else {
				new_container.push_back (container.at (cn_a));
				cn_a ++;
			}
		}

		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleArray> (new_container)
		));
	} else if (method == string {"appendall"}) {
		auto main_value = get_parameter ();
		auto main_capsule_value = dynamic_pointer_cast <CapsuleValue> (main_value);
		auto main_array_container = dynamic_pointer_cast <CapsuleArray> (main_capsule_value->value);
		deque <shared_ptr <Value>> main_array = main_array_container->container;
		
		auto new_container = container;

		for (auto main_value: main_array) {
			new_container.push_back (main_value);
		}

		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleArray> (new_container)
		));
	}
}


shared_ptr <CapsuleAppendable> CapsuleArray::append
	(shared_ptr <class Value> a_value)
{
	auto new_container = container;
	new_container.push_back (a_value);
	return make_shared <CapsuleArray> (new_container);
}


