#ifndef TODES_IMPL_H
#define TODES_IMPL


#include <string>
#include <map>
#include <vector>
#include <fstream>
#include <memory>
#include <set>
#include <deque>

#include "picojson.h"


namespace todes {


using namespace std;


/* Syntactic node classes */


class Expression: public Node {
public:
	virtual void read (picojson::object a_in) {
		/* Do nothing. */
	};
};


class ExpressionString: public Expression {
public:
	string value;
public:
	void read (picojson::object a_in) {
		value = a_in.at (string {"value"}).get <string> ();
	};
};


class ExpressionSpecial: public Expression {
public:
	string name;
public:
	void read (picojson::object a_in) {
		name = a_in.at (string {"name"}).get <string> ();
	};
};


class ExpressionParameter: public Expression {
public:
	size_t depth;
	string name;
public:
	void read (picojson::object a_in) {
		depth = static_cast <size_t> (a_in.at (string {"depth"}).get <double> ());
		name = a_in.at (string {"name"}).get <string> ();
	};
};


class ExpressionCompound: public Expression {
public:
	map <string, shared_ptr <class Expression>> parameters;
public:
	void read (picojson::object a_in);
};


shared_ptr <Expression> read_expression (picojson::value a_value);


}; /* namespace todes { */


#endif /* #ifndef TODES_IMPL_H */

