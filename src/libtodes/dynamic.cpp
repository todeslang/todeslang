#include <limits>
#include <sstream>

#include "todes.h"


using namespace std;
using namespace todes;


void DynamicEnvironment::bootstrap ()
{
	program_counter = 0;
	continuation_parameter = make_shared <BooleanValue> (false);
}


void DynamicEnvironment::dump (string &a_out) const
{
	stringstream stream;
	stream
		<< "{" << endl
		<< "\t" << "\"argumentResolutionEnvironmentDepth\":"
			<< argument_resolution_environment_stack.size () << "," << endl;
	if (! argument_resolution_environment_stack.empty ()) {
		stream
			<< "\t\"parameters\":[" << endl;
		auto parameters = argument_resolution_environment_stack.back ().current_parameters;
		for (auto parameter: parameters) {
			stream
				<< "\t\t{\"" << parameter.first << "\":\""
				<< parameter.second->format () << "\"}," << endl;
		}
		stream
			<< "\t]" << endl;
	}
	stream
		<< "}" << endl;
	a_out = stream.str ();
}


void DynamicEnvironment::dump (vector <map <string, shared_ptr <Value>>> &a_out) const
{
	vector <map <string, shared_ptr <Value>>> out;
	for (auto are: argument_resolution_environment_stack) {
		out.push_back (are.current_parameters);
	}
	a_out = out;
}


shared_ptr <class Value> DynamicEnvironment::run
        (shared_ptr <class Value> a_continuation_parameter)
{
	return static_environment->run (* this, a_continuation_parameter);
}


