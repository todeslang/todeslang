#include <sstream>
#include <ctime>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <random>
#include <iomanip>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <cmath>

#include "strsha256.h"

#include "todes.h"


using namespace std;
using namespace todes;


/* Operators */


bool todes::equal (shared_ptr <Value> left, shared_ptr <Value> right)
{
	auto number_left = dynamic_pointer_cast <NumberValue> (left);
	auto number_right = dynamic_pointer_cast <NumberValue> (right);
	auto string_left = dynamic_pointer_cast <StringValue> (left);
	auto string_right = dynamic_pointer_cast <StringValue> (right);
	auto boolean_left = dynamic_pointer_cast <BooleanValue> (left);
	auto boolean_right = dynamic_pointer_cast <BooleanValue> (right);
	/* ContinuationValue */
	auto terminal_continuation_left = dynamic_pointer_cast <TerminalContinuationValue> (left);
	auto terminal_continuation_right = dynamic_pointer_cast <TerminalContinuationValue> (right);
	/* CapsuleValue */
	auto pair_left = dynamic_pointer_cast <PairValue> (left);
	auto pair_right = dynamic_pointer_cast <PairValue> (right);
	auto broken_pair_left = dynamic_pointer_cast <BrokenPairValue> (left);
	auto broken_pair_right = dynamic_pointer_cast <BrokenPairValue> (right);
	auto float_left = dynamic_pointer_cast <FloatValue> (left);
	auto float_right = dynamic_pointer_cast <FloatValue> (right);

	return
		(number_left && number_right && number_left->value == number_right->value)
		|| (string_left && string_right && string_left->value == string_right->value)
		|| (boolean_left && boolean_right && boolean_left->value == boolean_right->value)
		|| (terminal_continuation_left && terminal_continuation_right)
		|| (pair_left
			&& pair_right
			&& equal (pair_left->key, pair_right->key)
			&& equal (pair_left->value, pair_right->value)
		   )
		|| (broken_pair_left
			&& broken_pair_right
			&& equal (broken_pair_left->key, broken_pair_right->key)
		   )
		|| (float_left && float_right && float_left->value == float_right->value);
}


class PrimitiveEqual: public Primitive {
public:
	void run () {
		set_return_value (make_shared <BooleanValue> (
			equal (get_parameter (string {"left"}), get_parameter (string {"right"}))
		));
	};
};


class PrimitiveLess: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto string_right = dynamic_pointer_cast <StringValue> (right);
		auto boolean_left = dynamic_pointer_cast <BooleanValue> (left);
		auto boolean_right = dynamic_pointer_cast <BooleanValue> (right);
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */
		auto float_left = dynamic_pointer_cast <FloatValue> (left);
		auto float_right = dynamic_pointer_cast <FloatValue> (right);

		bool return_value
		=	(number_left && number_right && number_left->value < number_right->value)
		||	(string_left && string_right && string_left->value < string_right->value)
		||	(boolean_left && boolean_right && (boolean_left->value? 1: 0) < (boolean_right->value? 1: 0))
		||	(float_left && float_right && float_left->value < float_right->value);
		
		set_return_value (make_shared <BooleanValue> (return_value));
	};
};


class PrimitiveLessOrEqual: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto string_right = dynamic_pointer_cast <StringValue> (right);
		auto boolean_left = dynamic_pointer_cast <BooleanValue> (left);
		auto boolean_right = dynamic_pointer_cast <BooleanValue> (right);
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */
		auto float_left = dynamic_pointer_cast <FloatValue> (left);
		auto float_right = dynamic_pointer_cast <FloatValue> (right);

		bool return_value
		=	(number_left && number_right && number_left->value <= number_right->value)
		||	(string_left && string_right && string_left->value <= string_right->value)
		||	(boolean_left && boolean_right && (boolean_left->value? 1: 0) <= (boolean_right->value? 1: 0))
		||	(float_left && float_right && float_left->value <= float_right->value);
		
		set_return_value (make_shared <BooleanValue> (return_value));
	};
};


class PrimitiveIs: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto string_right = dynamic_pointer_cast <StringValue> (right);
		if (! string_right) {
			return;
		}
		
		string name = string_right->value;

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto boolean_left = dynamic_pointer_cast <BooleanValue> (left);
		auto continuation_left = dynamic_pointer_cast <ContinuationValue> (left);
		auto terminal_continuation_left = dynamic_pointer_cast <ContinuationValue> (left);
		auto capsule_left = dynamic_pointer_cast <TerminalContinuationValue> (left);
		auto pair_left = dynamic_pointer_cast <PairValue> (left);
		auto broken_pair_left = dynamic_pointer_cast <BrokenPairValue> (left);
		auto float_left = dynamic_pointer_cast <FloatValue> (left);

		bool return_value
		=	(number_left && name == string {"number"})
		||	(string_left && name == string {"string"})
		||	(boolean_left && name == string {"boolean"})
		||	(continuation_left && name == string {"continuation"})
		||	(terminal_continuation_left && name == string {"terminalcontinuation"})
		||	(capsule_left && name == string {"capsule"})
		||	(pair_left && name == string {"pair"})
		||	(broken_pair_left && name == string {"brokenpair"})
		||	(float_left && name == string {"float"});
		
		set_return_value (make_shared <BooleanValue> (return_value));
	};
};


class PrimitiveModulo: public Primitive {
public:
	void run () {
		auto left_value = get_parameter (string {"left"});
		auto left_number_value = dynamic_pointer_cast <NumberValue> (left_value);
		auto right_value = get_parameter (string {"right"});
		auto right_number_value = dynamic_pointer_cast <NumberValue> (right_value);

		if (left_number_value && right_number_value) {
			auto left_number = left_number_value->value;
			auto right_number = right_number_value->value;
			set_return_value (make_shared <NumberValue> (
				left_number % right_number
			));
		}

		auto float_left = dynamic_pointer_cast <FloatValue> (left_value);
		auto float_right = dynamic_pointer_cast <FloatValue> (right_value);

		if (float_left && float_right) {
			set_return_value (make_shared <FloatValue> (
				fmod (float_left->value, float_right->value)
			));
		}

	};
};


class PrimitivePair: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});
		set_return_value (make_shared <PairValue> (left, right));
	};
};


class PrimitivePlus: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		/* StringValue */
		/* BooleanValue */
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */

		if (number_left && number_right) {
			set_return_value (make_shared <NumberValue> (
				number_left->value + number_right->value
			));
		}

		auto float_left = dynamic_pointer_cast <FloatValue> (left);
		auto float_right = dynamic_pointer_cast <FloatValue> (right);

		if (float_left && float_right) {
			set_return_value (make_shared <FloatValue> (
				float_left->value + float_right->value
			));
		}
	};
};


class PrimitiveMinus: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		/* StringValue */
		/* BooleanValue */
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */

		if (number_left && number_right) {
			set_return_value (make_shared <NumberValue> (
				number_left->value - number_right->value
			));
		}

		auto float_left = dynamic_pointer_cast <FloatValue> (left);
		auto float_right = dynamic_pointer_cast <FloatValue> (right);

		if (float_left && float_right) {
			set_return_value (make_shared <FloatValue> (
				float_left->value - float_right->value
			));
		}
	};
};


class PrimitiveMultiply: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		/* StringValue */
		/* BooleanValue */
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */

		if (number_left && number_right) {
			set_return_value (make_shared <NumberValue> (
				number_left->value * number_right->value
			));
		}

		auto float_left = dynamic_pointer_cast <FloatValue> (left);
		auto float_right = dynamic_pointer_cast <FloatValue> (right);

		if (float_left && float_right) {
			set_return_value (make_shared <FloatValue> (
				float_left->value * float_right->value
			));
		}
	};
};


class PrimitiveDivide: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto number_right = dynamic_pointer_cast <NumberValue> (right);
		/* StringValue */
		/* BooleanValue */
		/* ContinuationValue */
		/* TerminalContinuationValue */
		/* CapsuleValue */
		/* PairValue */
		/* BrokenPairValue */

		if (number_left && number_right) {
			set_return_value (make_shared <NumberValue> (
				number_left->value / number_right->value
			));
		}

		auto float_left = dynamic_pointer_cast <FloatValue> (left);
		auto float_right = dynamic_pointer_cast <FloatValue> (right);

		if (float_left && float_right) {
			set_return_value (make_shared <FloatValue> (
				float_left->value / float_right->value
			));
		}
	};
};


class PrimitiveAppend: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		/* NumberValue */
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto string_right = dynamic_pointer_cast <StringValue> (right);
		/* BooleanValue */
		/* ContinuationValue */
		/* TerminalContinuationValue */
		auto capsule_left = dynamic_pointer_cast <CapsuleValue> (left);

		if (string_left) {
			if (string_right) {
				set_return_value
					(make_shared <StringValue> (string_left->value + string_right->value));
			} else {
				set_return_value
					(make_shared <StringValue> (string_left->value + right->format ()));
			}
		} else if (capsule_left) {
			auto capsule_appendable = dynamic_pointer_cast
				<CapsuleAppendable> (capsule_left->value);
			set_return_value (
				make_shared <CapsuleValue> (capsule_appendable->append (right))
			);
		}
	};
};


class PrimitiveAs: public Primitive {
public:
	void run () {
		auto left = get_parameter (string {"left"});
		auto right = get_parameter (string {"right"});

		auto string_right = dynamic_pointer_cast <StringValue> (right);
		if (! string_right) {
			return;
		}
		
		string name = string_right->value;

		auto number_left = dynamic_pointer_cast <NumberValue> (left);
		auto string_left = dynamic_pointer_cast <StringValue> (left);
		auto boolean_left = dynamic_pointer_cast <BooleanValue> (left);
		auto continuation_left = dynamic_pointer_cast <ContinuationValue> (left);
		auto terminal_continuation_left = dynamic_pointer_cast <ContinuationValue> (left);
		auto capsule_left = dynamic_pointer_cast <TerminalContinuationValue> (left);
		auto pair_left = dynamic_pointer_cast <PairValue> (left);
		auto broken_pair_left = dynamic_pointer_cast <BrokenPairValue> (left);
		auto float_left = dynamic_pointer_cast <FloatValue> (left);

		if (name == string {"number"}) {
			if (number_left) {
				set_return_value (number_left);
			} else if (float_left) {
				set_return_value (make_shared <NumberValue> (static_cast <size_t> (float_left->value)));
			}
		} else if (name == string {"string"}) {
			if (string_left) {
				set_return_value (string_left);
			} else {
				set_return_value (make_shared <StringValue> (left->format ()));
			}
		} else if (name == string {"boolean"}) {
			set_return_value (make_shared <BooleanValue> (left->is_true ()));
		} else if (name == string {"continuation"}) {
			if (continuation_left) {
				set_return_value (continuation_left);
			}
		} else if (name == string {"terminalcontinuation"}) {
			if (terminal_continuation_left) {
				set_return_value (terminal_continuation_left);
			}
		} else if (name == string {"capsule"}) {
			if (capsule_left) {
				set_return_value (capsule_left);
			}
		} else if (name == string {"pair"}) {
			if (pair_left) {
				set_return_value (pair_left);
			}
		} else if (name == string {"brokenpair"}) {
			if (broken_pair_left) {
				set_return_value (broken_pair_left);
			}
		} else if (name == string {"float"}) {
			if (float_left) {
				set_return_value (float_left);
			} else if (number_left) {
				set_return_value (make_shared <FloatValue> (static_cast <double> (number_left->value)));
			} else if (string_left) {
				stringstream stream {string_left->value};
				double floating_point_number;
				stream >> floating_point_number;
				set_return_value (make_shared <FloatValue> (floating_point_number));
			}
		} else {
			/* Do nothing. */
		}
	};
};


class PrimitiveNot: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		set_return_value (make_shared <BooleanValue> (! main_value->is_true ()));
	};
};


class PrimitiveStringToNumber: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		auto main_string = dynamic_pointer_cast <StringValue> (main_value);
		if (! main_string) {
			return;
		}
		size_t number = 0;
		istringstream stream {main_string->value};
		stream >> hex >> number;
		set_return_value (make_shared <NumberValue> (number));
	};
};


class PrimitiveBrokenPair: public Primitive {
	void run () {
		auto main_value = get_parameter ();
		set_return_value (make_shared <BrokenPairValue> (main_value));
	};
};


/* Core */


class PrimitiveReleaseContinuation: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		auto main_continuation = dynamic_pointer_cast <ContinuationValue> (main_value);
		if (main_continuation) {
			size_t index = main_continuation->index;
			map <size_t, shared_ptr <DynamicEnvironment>> & continuations
				= main_continuation->static_environment->continuations;
			if (continuations.find (index) != continuations.end ()) {
				continuations.erase (index);
			}
		}
	};
};


class PrimitiveGetTerminalContinuation: public Primitive {
public:
	void run () {
		set_return_value (make_shared <TerminalContinuationValue> ());
	};
};


class PrimitiveArray: public Primitive {
public:
	void run () {
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleArray> ()
		));
	};
};


class PrimitiveSet: public Primitive {
public:
	void run () {
		Comparer comparer;
		auto continuation_main = dynamic_pointer_cast <ContinuationValue> (get_parameter ());
		if (continuation_main) {
			shared_ptr <DynamicEnvironment> env_p
				= continuation_main->get_dynamic_environment_ptr ();
			if (env_p) {
				comparer.continuation = env_p;
			}
		}
		auto container = make_shared <set <shared_ptr <Value>, Comparer>> (
			set <shared_ptr <Value>, Comparer> (comparer)
		);
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleSet> (container, comparer)
		));
	};
};


class PrimitiveMap: public Primitive {
public:
	void run () {
		Comparer comparer;
		auto continuation_main = dynamic_pointer_cast <ContinuationValue> (get_parameter ());
		if (continuation_main) {
			shared_ptr <DynamicEnvironment> env_p
				= continuation_main->get_dynamic_environment_ptr ();
			if (env_p) {
				comparer.continuation = env_p;
			}
		}
		auto container = make_shared <map <
			shared_ptr <Value>,
			shared_ptr <Value>,
			Comparer
		>> (
			comparer
		);
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleMap> (container, comparer, false)
		));
	};
};


class PrimitiveObject: public Primitive {
public:
	void run () {
		Comparer comparer;
		auto continuation_main = dynamic_pointer_cast <ContinuationValue> (get_parameter ());
		if (continuation_main) {
			shared_ptr <DynamicEnvironment> env_p
				= continuation_main->get_dynamic_environment_ptr ();
			if (env_p) {
				comparer.continuation = env_p;
			}
		}
		auto container = make_shared <map <
			shared_ptr <Value>,
			shared_ptr <Value>,
			Comparer
		>> (
			comparer
		);
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleMap> (container, comparer, true)
		));
	};
};


class PrimitiveTrivialContainer: public Primitive {
public:
	void run () {
		auto capsule = make_shared <CapsuleTrivialContainer> ();
		capsule->value = get_parameter ();
		set_return_value (make_shared <CapsuleValue> (capsule));
	};
};


class PrimitiveGetContinuation: public Primitive {
public:
	void run () {
		auto continuation_value = dynamic_environment->static_environment->haunted_continuation_cache;
		set_return_value (continuation_value);
	}
};


class PrimitiveGetContinuationParameter: public Primitive {
public:
	void run () {
		set_return_value (dynamic_environment->continuation_parameter);
	}
};


class PrimitiveThrowException: public Primitive {
public:
	void run () {
		string main_string {"Thrown by guest language."};
		auto main_value = get_parameter ();
		auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
		if (main_string_value) {
			main_string = main_string_value->value;
		}

		string file_string {__FILE__};
		auto file_value = get_parameter ("file");
		auto file_string_value = dynamic_pointer_cast <StringValue> (file_value);
		if (file_string_value) {
			file_string = file_string_value->value;
		}
		
		size_t line_number = __LINE__;
		auto line_value = get_parameter ("line");
		auto line_number_value = dynamic_pointer_cast <NumberValue> (line_value);
		if (line_number_value) {
			line_number = line_number_value->value;
		}
		
		throw (GuestCatchableException {main_string.c_str (), file_string.c_str (), line_number});
	}
};


class PrimitiveTry: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		auto main_continuation = dynamic_pointer_cast <ContinuationValue> (main_value);
		auto continuation_parameter_value = get_parameter ("continuationparameter");
		auto fallback_value = get_parameter ("fallback");
		
		set_return_value (fallback_value);
		
		if (main_continuation) {
			auto env_p = main_continuation->get_dynamic_environment_ptr ();
			if (env_p) {
				DynamicEnvironment env = (* env_p);
				try {
					auto terminal_continuation_value = env.run (continuation_parameter_value);
					set_return_value (terminal_continuation_value);
				} catch (exception &e) {
					cerr << e.what () << endl;
				}
			}
		}
	}
};


class PrimitiveEcho: public Primitive {
public:
	void run () {
		set_return_value (get_parameter ());
	}
};


class PrimitivePrint: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		auto main_string = dynamic_pointer_cast <StringValue> (main_value);
		if (main_string) {
			cout << main_string->value;
		} else {
			cout << main_value->format ();
		}
	}
};


/* File */


class PrimitiveFile: public Primitive {
public:
	void run () {
		set_return_value (make_shared <CapsuleValue> (
			make_shared <CapsuleFile> ()
		));
	}
};


/* Formatter */


class PrimitiveFormat: public Primitive {
public:
	void run () {
		stringstream s;

		s.imbue (std::locale::classic ());

		auto width_value = get_parameter ("width");
		auto width_number = dynamic_pointer_cast <NumberValue> (width_value);
		if (width_number) {
			s.width (width_number->value);
		}

		auto precision_value = get_parameter ("precision");
		auto precision_number = dynamic_pointer_cast <NumberValue> (precision_value);
		if (precision_number) {
			s.precision (precision_number->value);
		}

		if (get_parameter ("boolalpha")->is_true ()) {s << boolalpha; }
		if (get_parameter ("dec")->is_true ()) {s << dec; }
		if (get_parameter ("defaultfloat")->is_true ()) {s << defaultfloat; }
		if (get_parameter ("fixed")->is_true ()) {s << fixed; }
		if (get_parameter ("hex")->is_true ()) {s << hex; }
		if (get_parameter ("hexfloat")->is_true ()) {s << hexfloat; }
		if (get_parameter ("internal")->is_true ()) {s << internal; }
		if (get_parameter ("left")->is_true ()) {s << left; }
		if (get_parameter ("noboolalpha")->is_true ()) {s << noboolalpha; }
		if (get_parameter ("noshowbase")->is_true ()) {s << noshowbase; }
		if (get_parameter ("noshowpoint")->is_true ()) {s << noshowpoint; }
		if (get_parameter ("noshowpos")->is_true ()) {s << noshowpos; }
		if (get_parameter ("noskipws")->is_true ()) {s << noskipws; }
		if (get_parameter ("nounitbuf")->is_true ()) {s << nounitbuf; }
		if (get_parameter ("nouppercase")->is_true ()) {s << nouppercase; }
		if (get_parameter ("oct")->is_true ()) {s << oct; }
		if (get_parameter ("right")->is_true ()) {s << right; }
		if (get_parameter ("scientific")->is_true ()) {s << scientific; }
		if (get_parameter ("showbase")->is_true ()) {s << showbase; }
		if (get_parameter ("showpoint")->is_true ()) {s << showpoint; }
		if (get_parameter ("showpos")->is_true ()) {s << showpos; }
		if (get_parameter ("skipws")->is_true ()) {s << skipws; }
		if (get_parameter ("unitbuf")->is_true ()) {s << unitbuf; }
		if (get_parameter ("uppercase")->is_true ()) {s << uppercase; }
		
		auto main_value = get_parameter ();

		auto main_number = dynamic_pointer_cast <NumberValue> (main_value);
		if (main_number) {
			s << main_number->value;
		}
		
		auto main_float = dynamic_pointer_cast <FloatValue> (main_value);
		if (main_float) {
			s << main_float->value;
		}

		set_return_value (make_shared <StringValue> (s.str ()));
	};
};


class PrimitiveScan: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ();
		auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
		string main_string = main_string_value->value;

		stringstream s {main_string};

		s.imbue (std::locale::classic ());

		auto width_value = get_parameter ("width");
		auto width_number = dynamic_pointer_cast <NumberValue> (width_value);
		if (width_number) {
			s.width (width_number->value);
		}

		auto precision_value = get_parameter ("precision");
		auto precision_number = dynamic_pointer_cast <NumberValue> (precision_value);
		if (precision_number) {
			s.precision (precision_number->value);
		}

		if (get_parameter ("boolalpha")->is_true ()) {s << boolalpha; }
		if (get_parameter ("dec")->is_true ()) {s << dec; }
		if (get_parameter ("defaultfloat")->is_true ()) {s << defaultfloat; }
		if (get_parameter ("fixed")->is_true ()) {s << fixed; }
		if (get_parameter ("hex")->is_true ()) {s << hex; }
		if (get_parameter ("hexfloat")->is_true ()) {s << hexfloat; }
		if (get_parameter ("internal")->is_true ()) {s << internal; }
		if (get_parameter ("left")->is_true ()) {s << left; }
		if (get_parameter ("noboolalpha")->is_true ()) {s << noboolalpha; }
		if (get_parameter ("noshowbase")->is_true ()) {s << noshowbase; }
		if (get_parameter ("noshowpoint")->is_true ()) {s << noshowpoint; }
		if (get_parameter ("noshowpos")->is_true ()) {s << noshowpos; }
		if (get_parameter ("noskipws")->is_true ()) {s << noskipws; }
		if (get_parameter ("nounitbuf")->is_true ()) {s << nounitbuf; }
		if (get_parameter ("nouppercase")->is_true ()) {s << nouppercase; }
		if (get_parameter ("oct")->is_true ()) {s << oct; }
		if (get_parameter ("right")->is_true ()) {s << right; }
		if (get_parameter ("scientific")->is_true ()) {s << scientific; }
		if (get_parameter ("showbase")->is_true ()) {s << showbase; }
		if (get_parameter ("showpoint")->is_true ()) {s << showpoint; }
		if (get_parameter ("showpos")->is_true ()) {s << showpos; }
		if (get_parameter ("skipws")->is_true ()) {s << skipws; }
		if (get_parameter ("unitbuf")->is_true ()) {s << unitbuf; }
		if (get_parameter ("uppercase")->is_true ()) {s << uppercase; }
		
		auto type_value = get_parameter ("type");
		auto type_string_value = dynamic_pointer_cast <StringValue> (type_value);
		string type_string = type_string_value->value;

		if (type_string == string {"number"}) {
			size_t the_number;
			s >> the_number;
			set_return_value (make_shared <NumberValue> (the_number));
		} else if (type_string == string {"float"}) {
			double the_float;
			s >> the_float;
			set_return_value (make_shared <FloatValue> (the_float));
		} else {
			throw (GuestCatchableException {"PrimitiveScan: unknown type.", __FILE__, __LINE__});
		}
	};
};


/* External */


class PrimitiveExec: public Primitive {
public:
	void run ();
};


void PrimitiveExec::run ()
{
	auto main_value = get_parameter ("main");
	auto main_capsule_value = dynamic_pointer_cast <CapsuleValue> (main_value);
	auto main_capsule = main_capsule_value->value;
	auto main_capsule_array = dynamic_pointer_cast <CapsuleArray> (main_capsule);
	auto main_array = main_capsule_array->container;
	vector <string> main_vector_string;
	for (auto item_value: main_array) {
		auto item_string_value = dynamic_pointer_cast <StringValue> (item_value);
		string item_string = item_string_value->value;
		main_vector_string.push_back (item_string);
	}
	
	pid_t pid = fork ();
	
	if (pid == 0) {
		wait (nullptr);
	} else {
		auto argv = (char **) malloc (sizeof (char *) * (main_vector_string.size () + 1)); /* Never delete */
		for (size_t cn = 0; cn < main_vector_string.size (); cn ++) {
			argv [cn] = (char *) main_vector_string.at (cn).c_str ();
		}
		argv [main_vector_string.size ()] = nullptr;
		execvp (main_vector_string.front ().c_str (), (char * const *) argv);
		exit (1);
	}
}


class PrimitiveCreateTemporaryFile: public Primitive {
public:
	default_random_engine random_engine;
public:
	PrimitiveCreateTemporaryFile () {
		random_engine.seed (random_device {} ());
	};
	void run ();
};


void PrimitiveCreateTemporaryFile::run ()
{
	string suffix;
	
	auto main_value = get_parameter ("main");
	auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
	if (main_string_value) {
		suffix = main_string_value->value;
	}

	string random_string;
	for (int cn = 0; cn < 16; cn ++) {
		stringstream stream;
		uniform_int_distribution <uint64_t> distribution {0, 0x10000};
		uint64_t random_number = distribution (random_engine);
		stream << setfill ('0') << right << setw (4) << hex << random_number;
		random_string += stream.str ();
	}

	string path = string {"/tmp/"} + random_string + suffix;

	set_return_value (make_shared <StringValue> (path));
	
	int fd = open (path.c_str (), O_CREAT | O_EXCL);
	
	fchmod (fd, S_IRUSR | S_IWUSR);
	
	close (fd);
}


class PrimitiveRemoveFile: public Primitive {
public:
	void run ();
};


void PrimitiveRemoveFile::run ()
{
	auto main_value = get_parameter ("main");
	auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
	if (main_string_value) {
		string path = main_string_value->value;
		unlink (path.c_str ());
	}
}


/* Misc (out of specification) */


class PrimitiveGetDigitsOfNumberType: public Primitive {
public:
	void run () {
		set_return_value (make_shared <NumberValue> (
			numeric_limits<size_t>::digits
		));
	};
};


class PrimitiveGetTime: public Primitive {
public:
	void run () {
		set_return_value (make_shared <NumberValue> (
			static_cast <size_t> (time (nullptr))
		));
	};
};


static vector <vector <string>> decode_csv (string csv)
{
	vector <vector <string>> table;
	vector <string> record;
	string field;

	uint64_t state = 0;

	for (auto c: csv) {
		switch (state) {
		case 0:
			if (c == '"') {
				state = 1;
			} else if (c == ',') {
				record.push_back (field);
				field.clear ();
				state = 0;
			} else if (c == '\n') {
				record.push_back (field);
				field.clear ();
				table.push_back (record);
				record.clear ();
				state = 0;
			} else if (c == '\r') {
				/* Do nothing. */
			} else {
				field.push_back (c);
			}
			break;
		case 1:
			if (c == '"') {
				state = 2;
			} else if (c == '\r') {
				/* Do nothing. */
			} else {
				field.push_back (c);
			}
			break;
		case 2:
			if (c == '"') {
				field.push_back ('"');
				state = 1;
			} else if (c == ',') {
				record.push_back (field);
				field.clear ();
				state = 0;
			} else if (c == '\n') {
				record.push_back (field);
				field.clear ();
				table.push_back (record);
				record.clear ();
				state = 0;
			} else if (c == '\r') {
				/* Do nothing. */
			} else {
				field.push_back (c);
				state = 0;
			}
			break;
		default:
			abort ();
			break;
		}
	}
	
	if (! field.empty ()) {
		record.push_back (field);
	}

	if (! record.empty ()) {
		table.push_back (record);
	}
	
	return table;
}


class PrimitiveDecodeCsv: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ("main");
		auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
		string main_string = main_string_value->value;
		vector <vector <string>> table = decode_csv (main_string);
		
		vector <shared_ptr <Value>> table_internal;
		
		for (vector <string> record: table) {
			vector <shared_ptr <Value>> record_internal;
			for (string field: record) {
				auto field_value = make_shared <StringValue> (field);
				record_internal.push_back (field_value);
			}
			auto record_capsule = make_shared <CapsuleArray> (record_internal);
			auto record_value = make_shared <CapsuleValue> (record_capsule);
			table_internal.push_back (record_value);
		}

		auto table_capsule = make_shared <CapsuleArray> (table_internal);
		auto table_value = make_shared <CapsuleValue> (table_capsule);
		
		set_return_value (table_value);
	};
};


static string escape_attribute (string in)
{
	string out;
	for (auto c: in) {
		if (c == '"') {
			out += string {"&quot;"};
		} else if (c == '&') {
			out += string {"&amp;"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


class PrimitiveEscapeAttribute: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ("main");
		auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
		string main_string = main_string_value->value;
		set_return_value (make_shared <StringValue> (
			escape_attribute (main_string)
		));
	};
};


static string escape_html (string in)
{
	string out;
	for (auto c: in) {
		if (c == '<') {
			out += string {"&lt;"};
		} else if (c == '>') {
			out += string {"&gt;"};
		} else if (c == '&') {
			out += string {"&amp;"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


class PrimitiveEscapeHtml: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ("main");
		auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
		string main_string = main_string_value->value;
		set_return_value (make_shared <StringValue> (
			escape_html (main_string)
		));
	};
};


static string escape_csv (string in)
{
	string out;
	for (auto c: in) {
		if (c == '"') {
			out += string {"\"\""};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


class PrimitiveEscapeCsv: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ("main");
		auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
		string main_string = main_string_value->value;
		set_return_value (make_shared <StringValue> (
			escape_csv (main_string)
		));
	};
};


static bool starts_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (0, y.size ()) == y;
}


class PrimitiveStartsWith: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ("main");
		auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
		string main_string = main_string_value->value;

		auto with_value = get_parameter ("with");
		auto with_string_value = dynamic_pointer_cast <StringValue> (with_value);
		string with_string = with_string_value->value;

		set_return_value (make_shared <BooleanValue> (
			starts_with (main_string, with_string)
		));
	};
};


static bool ends_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (x.size () - y.size ()) == y;
}


class PrimitiveEndsWith: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ("main");
		auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
		string main_string = main_string_value->value;

		auto with_value = get_parameter ("with");
		auto with_string_value = dynamic_pointer_cast <StringValue> (with_value);
		string with_string = with_string_value->value;

		set_return_value (make_shared <BooleanValue> (
			ends_with (main_string, with_string)
		));
	};
};


class PrimitiveGetSha256String: public Primitive {
public:
	void run () {
		auto main_value = get_parameter ("main");
		auto main_string_value = dynamic_pointer_cast <StringValue> (main_value);
		string main_string = main_string_value->value;

		strsha256::StrSha256 strsha256;
		
		set_return_value (make_shared <StringValue> (
			strsha256 (main_string)
		));
	};
};


class PrimitiveGetRandomString: public Primitive {
public:
	strsha256::StrRnd256 strrnd256;
public:
	void run () {
		set_return_value (make_shared <StringValue> (
			strrnd256 ()
		));
	};
};


class PrimitiveRandom: public Primitive {
public:
	void run () {
		set_return_value (
			make_shared <CapsuleValue> (
				make_shared <CapsuleRandom> ()
			)
		);
	};
};


/* Registration */


map <string, shared_ptr <Primitive>> todes::get_primitives ()
{
	return map <string, shared_ptr <Primitive>> {
		/* Operators */
		{string {"equal"}, make_shared <PrimitiveEqual> ()},
		{string {"less"}, make_shared <PrimitiveLess> ()},
		{string {"lessorequal"}, make_shared <PrimitiveLessOrEqual> ()},
		{string {"is"}, make_shared <PrimitiveIs> ()},
		{string {"modulo"}, make_shared <PrimitiveModulo> ()},
		{string {"pair"}, make_shared <PrimitivePair> ()},
		{string {"plus"}, make_shared <PrimitivePlus> ()},
		{string {"minus"}, make_shared <PrimitiveMinus> ()},
		{string {"multiply"}, make_shared <PrimitiveMultiply> ()},
		{string {"divide"}, make_shared <PrimitiveDivide> ()},
		{string {"append"}, make_shared <PrimitiveAppend> ()},
		{string {"as"}, make_shared <PrimitiveAs> ()},
		{string {"not"}, make_shared <PrimitiveNot> ()},
		{string {"stringtonumber"}, make_shared <PrimitiveStringToNumber> ()},
		{string {"brokenpair"}, make_shared <PrimitiveBrokenPair> ()},

		/* Core */
		{string {"releasecontinuation"}, make_shared <PrimitiveReleaseContinuation> ()},
		{string {"getterminalcontinuation"}, make_shared <PrimitiveGetTerminalContinuation> ()},
		{string {"Array"}, make_shared <PrimitiveArray> ()},
		{string {"Set"}, make_shared <PrimitiveSet> ()},
		{string {"Map"}, make_shared <PrimitiveMap> ()},
		{string {"Object"}, make_shared <PrimitiveObject> ()},
		{string {"Trivialcontainer"}, make_shared <PrimitiveTrivialContainer> ()},
		{string {"getcontinuation"}, make_shared <PrimitiveGetContinuation> ()},
		{string {"getcontinuationparameter"}, make_shared <PrimitiveGetContinuationParameter> ()},
		{string {"throwexception"}, make_shared <PrimitiveThrowException> ()},
		{string {"try"}, make_shared <PrimitiveTry> ()},
		{string {"echo"}, make_shared <PrimitiveEcho> ()},
		{string {"print"}, make_shared <PrimitivePrint> ()},
		
		/* File */
		{string {"File"}, make_shared <PrimitiveFile> ()},

		/* Format */
		{string {"format"}, make_shared <PrimitiveFormat> ()},
		{string {"scan"}, make_shared <PrimitiveScan> ()},

		/* External */
		{string {"exec"}, make_shared <PrimitiveExec> ()},
		{string {"createtemporaryfile"}, make_shared <PrimitiveCreateTemporaryFile> ()},
		{string {"removefile"}, make_shared <PrimitiveRemoveFile> ()},

		/* Misc (out of specification) */
		{string {"getdigitsofnumbertype"}, make_shared <PrimitiveGetDigitsOfNumberType> ()},
		{string {"gettime"}, make_shared <PrimitiveGetTime> ()},
		{string {"decodecsv"}, make_shared <PrimitiveDecodeCsv> ()},
		{string {"escapeattribute"}, make_shared <PrimitiveEscapeAttribute> ()},
		{string {"escapehtml"}, make_shared <PrimitiveEscapeHtml> ()},
		{string {"escapecsv"}, make_shared <PrimitiveEscapeCsv> ()},
		{string {"startswith"}, make_shared <PrimitiveStartsWith> ()},
		{string {"endswith"}, make_shared <PrimitiveEndsWith> ()},
		{string {"getsha256string"}, make_shared <PrimitiveGetSha256String> ()},
		{string {"getrandomstring"}, make_shared <PrimitiveGetRandomString> ()},
		{string {"Random"}, make_shared <PrimitiveRandom> ()},
	};
}


