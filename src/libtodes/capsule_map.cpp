#include "todes.h"


using namespace std;
using namespace todes;


void CapsuleMap::run ()
{
	auto method_value = get_parameter (string {"method"});
	auto method_string = dynamic_pointer_cast <StringValue> (method_value);
	if (! method_string) {
		return;
	}
	string method = method_string->value;

	auto main_value = get_parameter ();
	
	if (method == string {"Map"}) {
		set_return_value (make_shared <BooleanValue> (! is_object));
	} else if (method == string {"Object"}) {
		set_return_value (make_shared <BooleanValue> (is_object));
	} else if (method == string {"size"}) {
		set_return_value (make_shared <NumberValue> (container->size ()));
	} else if (method == string {"has"}) {
		set_return_value (make_shared <BooleanValue> (
			container->find (main_value) != container->end ()
		));
	} else if (method == string {"at"}) {
		if (container->find (main_value) != container->end ()) {
			set_return_value (container->at (main_value));
		}
	} else if (method == string ("first")) {
		auto first = container->begin ();
		if (first != container->end ()) {
			set_return_value (first->first);
		}
	} else if (method == string ("next")) {
		auto found = container->find (main_value);
		if (found != container->end ()) {
			found ++;
			if (found != container->end ()) {
				set_return_value (found->first);
			}
		}
	}
}


shared_ptr <CapsuleAppendable>
	CapsuleMap::append (shared_ptr <class Value> a_value)
{
	auto new_container = make_shared <map <
		shared_ptr <Value>,
		shared_ptr <Value>,
		Comparer
	>> (comparer);

	new_container->insert (container->begin (), container->end ());

	auto pair_value = dynamic_pointer_cast <PairValue> (a_value);
	auto broken_pair_value = dynamic_pointer_cast <BrokenPairValue> (a_value);

	if (pair_value) {
		if (new_container->find (pair_value->key) == new_container->end ()) {
			new_container->insert (pair <shared_ptr <Value>, shared_ptr <Value>> {
				pair_value->key,
				pair_value->value
			});
		} else {
			new_container->at (pair_value->key) = pair_value->value;
		}
	}

	if (broken_pair_value) {
		if (new_container->find (broken_pair_value->key) != new_container->end ()) {
			new_container->erase (broken_pair_value->key);
		}
	}

	return make_shared <CapsuleMap> (new_container, comparer, is_object);
}


