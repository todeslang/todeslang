#include <functional>

#include "todes.h"


using namespace std;
using namespace todes;


todes::CapsuleRandom::CapsuleRandom ()
{
	random_device device;
	generator.seed (device ());
}


void CapsuleRandom::run ()
{
	auto method_value = get_parameter (string {"method"});
	auto method_string = dynamic_pointer_cast <StringValue> (method_value);
	if (! method_string) {
		return;
	}
	string method = method_string->value;

	if (method == string {"getnumber"}) {
		auto main_value = get_parameter ();
		auto main_number_value = dynamic_pointer_cast <NumberValue> (main_value);
		size_t main_number = main_number_value->value;
		uniform_int_distribution <size_t> distribution (0, main_number);
		auto roll = bind (distribution, generator);
		size_t random_number = roll ();
		set_return_value (make_shared <NumberValue> (random_number));
	} else if (method == string {"getfloat"}) {
		auto main_value = get_parameter ();
		auto main_float_value = dynamic_pointer_cast <FloatValue> (main_value);
		double main_float = main_float_value->value;
		uniform_real_distribution <double> distribution (static_cast <double> (0), main_float);
		auto roll = bind (distribution, generator);
		double random_float = roll ();
		set_return_value (make_shared <FloatValue> (random_float));
	}
}

