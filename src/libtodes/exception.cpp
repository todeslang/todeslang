#include <sstream>

#include "todes.h"


using namespace std;
using namespace todes;


GuestCatchableException::GuestCatchableException
	(const char *a_description, const char *a_file, size_t a_line)
{
	stringstream format;
	format
		<< a_description
		<< " ("
		<< a_file
		<< ", "
		<< a_line
		<< ")";
	what_cache = format.str ();
}


const char * GuestCatchableException::what () const noexcept
{
	return what_cache.c_str ();
};


