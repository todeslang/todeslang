#include "todes.h"


using namespace std;
using namespace todes;


void CapsuleTrivialContainer::run () {
	auto method_value = get_parameter ("method");
	auto method_string = dynamic_pointer_cast <StringValue> (method_value);
	if (! method_string) {
		return;
	}
	string method = method_string->value;

	if (method == string {"Trivialcontainer"}) {
		set_return_value (make_shared <BooleanValue> (true));
	} else if (method == string {"get"}) {
		set_return_value (value);
	} else if (method == string {"set"}) {
		value = get_parameter ();
	}
};


