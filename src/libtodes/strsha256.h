#ifndef STRSHA256_H
#define STRSHA256_H


#include <crypto++/cryptlib.h>
#include <crypto++/sha.h>
#include <string>
#include <sstream>
#include <random>


namespace strsha256 {


using namespace std;


class StrSha256 {
public:
	bool upper_case;
public:
	StrSha256 ():
		upper_case (false) 
		{ };
	StrSha256 (bool a_upper_case):
		upper_case (a_upper_case)
		{ };
public:
	string operator () (string in_string) {
		CryptoPP::SHA256 sha256;
		size_t in_length = in_string.size ();
		auto in_bytes = reinterpret_cast <const unsigned char *> (in_string.c_str ());
		auto out_length = sha256.DigestSize ();
		auto out_bytes = static_cast <unsigned char *> (malloc (out_length));
		sha256.CalculateDigest (out_bytes, in_bytes, in_length);
		string out_string;
		for (unsigned int cn = 0; cn < out_length; cn ++) {
			auto b = out_bytes [cn];
			auto b_unsigned_int = static_cast <unsigned int> (b);
			stringstream stream;
			stream << hex;
			stream << (upper_case? uppercase: nouppercase);
			stream.width (2);
			stream << right;
			stream.fill ('0');
			stream << b_unsigned_int;
			out_string += stream.str ();
		}
		free (out_bytes);
		return out_string;
	};
};


class StrRnd256 {
public:
	bool upper_case;
	default_random_engine generator;
public:
	StrRnd256 ():
		upper_case (false)
	{
		random_device device;
		generator.seed (device ());
	};
	StrRnd256 (bool a_upper_case):
		upper_case (a_upper_case)
	{
		random_device device;
		generator.seed (device ());
	};
public:
	string operator () () {
		uniform_int_distribution <unsigned int> distribution (0, 0x1000);
		auto roll = bind (distribution, generator);
		string s;
		for (int cn = 0; cn < 16; cn ++) {
			auto n = roll ();
			stringstream stream;
			stream << hex;
			stream << (upper_case? uppercase: nouppercase);
			stream.width (4);
			stream << right;
			stream.fill ('0');
			stream << n;
			s += stream.str ();
		}
		return s;
	};
};


}; /* namespace strsha256 { */


#endif /* #ifndef STRSHA256_H */

