#include <sstream>

#include "todes.h"


using namespace std;
using namespace todes;


string NumberValue::format () const
{
	stringstream stream;
	stream << hex << value;
	return string {"#"} + stream.str ();
}


string ContinuationValue::format () const
{
	stringstream stream;
	stream << index;
	return string {"(continuation: "} + stream.str () + string {")"};
}


shared_ptr <Value> ContinuationValue::run
	(shared_ptr <Value> a_continuation_parameter)
{
	auto return_value = make_shared <BooleanValue> (false);
	shared_ptr <DynamicEnvironment> env_p = get_dynamic_environment_ptr ();
	if (env_p) {
		DynamicEnvironment env = (* env_p);
		return env.run (a_continuation_parameter);
	}
	return return_value;
}


shared_ptr <class DynamicEnvironment> ContinuationValue::get_dynamic_environment_ptr ()
{
	map <size_t, shared_ptr <DynamicEnvironment>> & continuations
		= static_environment->continuations;
	shared_ptr <DynamicEnvironment> return_value;
	if (continuations.find (index) != continuations.end ()) {
		return_value = continuations.at (index);
	}
	return return_value;
}


string FloatValue::format () const
{
	stringstream stream;
	stream << hexfloat << value;
	return stream.str ();
}


