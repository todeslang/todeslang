#include <limits>

#include "todes.h"


using namespace std;
using namespace todes;


void StaticEnvironment::initialize ()
{
	primitives = get_primitives ();
}


shared_ptr <Value> StaticEnvironment::run ()
{
	DynamicEnvironment env;
	env.static_environment = this;
	env.bootstrap ();
	return run (env, make_shared <BooleanValue> (false));
}


shared_ptr <Value> StaticEnvironment::run (
	DynamicEnvironment a_env,
	shared_ptr <Value> a_continuation_parameter
)
{
	a_env.continuation_parameter = a_continuation_parameter;

	string env_dump;

	for (; ; ) {
		if (a_env.program_counter == numeric_limits <size_t>::max ()) {
			return a_env.working_register;
		}

		/*
			auto env_dump_new = a_env.dump ();
			if (env_dump != env_dump_new) {
				cout << env_dump_new;
				env_dump = env_dump_new;
			}
		*/

		auto instruction = program.instructions.at (a_env.program_counter);

		// cout << a_env.program_counter << " " << instruction->format () << endl;

		a_env.program_counter ++;
		instruction->run (a_env);
	}
}


