#include <vector>
#include <cassert>
#include <map>
#include <iostream>

#include "thel.h"


using namespace thel;
using namespace std;


static bool is_space_character (char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}


/*
	Directive := FullDirective / EasyDirective
*/
shared_ptr <NodeDirective> thel::parseDirective
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	{
		unsigned int eaten = 0;
		bool ok = false;
		auto node = parseFullDirective (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return node;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		auto node = parseEasyDirective (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return node;
		}
	}

	a_eaten = 0;
	a_ok = false;
	return shared_ptr <NodeDirective> ();
}


/*
	Directive := DirectiveName Space ";" Space Parameter (Space Parameter)*
*/
shared_ptr <NodeDirective> thel::parseFullDirective
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;

	string directive_name;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		directive_name = parseDirectiveName (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <NodeDirective> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {";"});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <NodeDirective> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	map <string, shared_ptr <Expression>> parameters;

	{
		unsigned int eaten = 0;
		bool ok = false;
		string parameter_name;
		shared_ptr <Expression> parameter_value;
		parseParameter (a_code, a_index + accumulator, eaten, ok, parameter_name, parameter_value);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <NodeDirective> {};
		}
		parameters.insert (pair <string, shared_ptr <Expression>> {
			parameter_name, parameter_value
		});
		accumulator += eaten;
	}

	for (; ; ) {
		unsigned int accumulator_in_loop = 0;
		{
			unsigned int eaten = 0;
			bool ok = false;
			parseSpace
				(a_code, a_index + accumulator + accumulator_in_loop, eaten, ok);
			assert (ok);
			accumulator_in_loop += eaten;
		}

		{
			unsigned int eaten = 0;
			bool ok = false;
			string parameter_name;
			shared_ptr <Expression> parameter_value;
			parseParameter (
				a_code,
				a_index + accumulator + accumulator_in_loop,
				eaten,
				ok,
				parameter_name,
				parameter_value
			);
			if (! ok) {
				break;
			}
			parameters.insert (pair <string, shared_ptr <Expression>> {
				parameter_name, parameter_value
			});
			accumulator_in_loop += eaten;
		}
		
		accumulator += accumulator_in_loop;
	}

	a_eaten = accumulator;
	a_ok = true;
	auto node_directive = make_shared <NodeDirective> ();
	node_directive->name = directive_name;
	node_directive->parameters = parameters;
	return node_directive;
}

/*
	DirectiveName := "step" / "anchor" / "suggest"
*/
string thel::parseDirectiveName
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	vector <string> directive_names {
		string {"step"},
		string {"continuation"},
		string {"anchor"},
	};
	
	return parseLiterals (a_code, a_index, a_eaten, a_ok, directive_names);
}


string thel::parseLiterals
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok, vector <string> a_literals)
{
	for (auto literal: a_literals) {
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index, eaten, ok, literal);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return literal;
		}
	}
	
	a_eaten = 0;
	a_ok = false;
	return string {};
}


void thel::parseSpace (string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;
	for (;
		a_index + accumulator < a_code.size ();
		accumulator ++)
	{
		auto c = a_code.at (a_index + accumulator);
		if (! is_space_character (c)) {
			break;
		}
	}
	a_eaten = accumulator;
	a_ok = true;
}


void thel::parseLiteral
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok, string a_literal)
{
	if (a_code.size () - a_index < a_literal.size ()) {
		a_eaten = 0;
		a_ok = false;
		return;
	}

	for (unsigned int cn = 0; cn < a_literal.size (); cn ++) {
		if (a_code.at (a_index + cn) != a_literal.at (cn)) {
			a_eaten = 0;
			a_ok = false;
			return;
		}
	}

	a_eaten = a_literal.size ();
	a_ok = true;
}


/*
	Parameter := String Space ":" Space Expression Space ";"
*/
void thel::parseParameter (
	string a_code,
	unsigned int a_index,
	unsigned int &a_eaten,
	bool &a_ok,
	string &a_parameter_name,
	shared_ptr <Expression> &a_parameter_value
)
{
	unsigned int accumulator = 0;
	
	string parameter_name;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		parameter_name = parseString (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return;
		}
		accumulator += eaten;
	}
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {":"});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return;
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	shared_ptr <Expression> parameter_value;

	{
		unsigned int eaten = 0;
		bool ok = false;
		parameter_value = parseExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return;
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {";"});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return;
		}
		accumulator += eaten;
	}

	a_eaten = accumulator;
	a_ok = true;
	a_parameter_name = parameter_name;
	a_parameter_value = parameter_value;
}


/*
EasyDirective := Expression Space ";" Space Expression
*/
shared_ptr <NodeDirective> thel::parseEasyDirective
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;
	
	shared_ptr <Expression> expression_if;
	shared_ptr <Expression> expression_parameters;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		expression_if = parseExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <NodeDirective> ();
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {";"});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <NodeDirective> ();
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		expression_parameters = parseExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <NodeDirective> ();
		}
		accumulator += eaten;
	}

	a_eaten = accumulator;
	a_ok = true;
	auto node_directive = make_shared <NodeDirective> ();
	node_directive->name = string {"step"};
	node_directive->parameters = map <string, shared_ptr <class Expression>> {
		{string {"if"}, expression_if},
		{string {"parameters"}, expression_parameters},
	};
	return node_directive;
}


/*
String := NominalString / LiteralString / SpecialString
*/
string thel::parseString (string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	{
		unsigned int eaten = 0;
		bool ok = false;
		string nominal_string = parseNominalString (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = ok;
			return nominal_string;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		string literal_string = parseLiteralString (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = ok;
			return literal_string;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		string special_string = parseSpecialString (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = ok;
			return special_string;
		}
	}
	
	a_eaten = 0;
	a_ok = false;
	return string {};
}

/*
NominalString := (! SpaceCharacter)
	("a"..."z" / "A"..."Z" / "0"..."9" / SpaceCharacter)*
	("a"..."z" / "A"..."Z" / "0"..."9")
*/
string thel::parseNominalString (string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	if (a_code.size () <= a_index
		|| is_space_character (a_code.at (a_index))
	) {
		a_eaten = 0;
		a_ok = false;
		return string {};
	}

	unsigned int accumulator = 0;
	string nominal_string;
	
	for (unsigned int cn = 0; a_index + cn < a_code.size (); cn ++) {
		auto c = a_code.at (a_index + cn);
		if (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || ('0' <= c && c <= '9')) {
			nominal_string.push_back (c);
			accumulator = cn;
		} else if (is_space_character (c)) {
			/* Do nothing, */
		} else {
			break;
		}
	}

	if (nominal_string.empty ()) {
		a_eaten = 0;
		a_ok = false;
		return string {};
	}

	a_eaten = accumulator + 1;
	a_ok = true;
	return nominal_string;
}


/*
LiteralString := "{" ((! "}") VisibleCharacter)+ "}"
*/
string thel::parseLiteralString (string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	if (a_code.size () < a_index + 3) {
		a_eaten = 0;
		a_ok = false;
		return string {};
	}

	unsigned int accumulator = 0;

	if (a_code.at (a_index + accumulator) != '{') {
		a_eaten = 0;
		a_ok = false;
		return string {};
	}

	accumulator ++;
	string literal_string;

	for (; a_index + accumulator < a_code.size (); accumulator ++) {
		char c = a_code.at (a_index + accumulator);
		auto c_unsigned_int = static_cast <unsigned int> (static_cast <unsigned char> (c));
		if (c == '}') {
			a_eaten = accumulator + 1;
			a_ok = true;
			return literal_string;
		} else if (0x20 <= c_unsigned_int && c_unsigned_int <= 0xff) {
			literal_string.push_back (c);
		} else {
			break;
		}
	}

	a_eaten = 0;
	a_ok = false;
	return string {};
}


/*
SpecialString := "^@" / "^?" / "^!" / "^&" / "^$" / "^file" / "^line"
*/
string thel::parseSpecialString (string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	vector <string> special_strings = {
		string {"^@"},
		string {"^?"},
		string {"^!"},
		string {"^&"},
		string {"^$"},
		string {"^file"},
		string {"^line"},
	};
	
	unsigned int eaten = 0;
	bool ok = false;
	string code = parseLiterals (a_code, a_index, eaten, ok, special_strings);
	if (! ok) {
		a_eaten = 0;
		a_ok = false;
		return string {};
	}
	
	map <string, string> code_to_semantics = {
		{string {"^@"}, string {}},
		{string {"^?"}, string {"\r"}},
		{string {"^!"}, string {"\n"}},
		{string {"^&"}, string {"\t"}},
		{string {"^$"}, string {"}"}},
		{string {"^file"}, g_file},
		{string {"^line"}, g_line},
	};
	
	a_eaten = eaten;
	a_ok = true;
	return code_to_semantics.at (code);
}



shared_ptr <Expression> thel::parseExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	return parseExpressionWithCache (a_code, a_index, a_eaten, a_ok);
}


shared_ptr <Expression> thel::parseExpressionWithCache
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	CacheInput cache_input;
	cache_input.code = a_code;
	cache_input.index = a_index;
	
	if (g_cache.find (cache_input) == g_cache.end ()) {
		auto expression = parseExpressionImpl (a_code, a_index, a_eaten, a_ok);
		CacheOutput cache_output;
		cache_output.expression = expression;
		cache_output.eaten = a_eaten;
		cache_output.ok = a_ok;
		g_cache.insert (pair <CacheInput, CacheOutput> {cache_input, cache_output});
		return expression;
	} else {
		CacheOutput cache_output = g_cache.at (cache_input);
		a_eaten = cache_output.eaten;
		a_ok = cache_output.ok;
		return cache_output.expression;
	}

	return shared_ptr <Expression> {};
}


/*
	Expression := ThenElseOperatorExpression
*/
shared_ptr <Expression> thel::parseExpressionImpl
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	return parseThenElseOperatorExpression (a_code, a_index, a_eaten, a_ok);
}


/*
	ThenElseOperatorExpression := ExplicitThenElseOperatorExpression / RightJoinOperatorExpression
*/
shared_ptr <Expression> thel::parseThenElseOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExplicitThenElseOperatorExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseRightJoinOperatorExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}
	
	a_eaten = 0;
	a_ok = false;
	return shared_ptr <Expression> {};
}

/*
	ExplicitNoJoinOperatorExpression
		:= RightJoinOperatorExpression Space 
		"^then" Space 
		RightJoinOperatorExpression Space
		"^else" Space
		RightJoinOperatorExpression
*/
shared_ptr <ExpressionCompound> thel::parseExplicitThenElseOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;
	
	shared_ptr <Expression> expression_if;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		expression_if = parseRightJoinOperatorExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {"^then"});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}
	
	shared_ptr <Expression> expression_then;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		expression_then = parseRightJoinOperatorExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {"^else"});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}
	
	shared_ptr <Expression> expression_else;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		expression_else = parseRightJoinOperatorExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	a_eaten = accumulator;
	a_ok = true;
	
	auto expression = make_shared <ExpressionCompound> ();
	
	expression->parameters = map <string, shared_ptr <Expression>> {
		{string {"action"}, make_shared <ExpressionString> (string {"if"})},
		{string {"if"}, expression_if},
		{string {"then"}, expression_then},
		{string {"else"}, expression_else},
	};
	
	return expression;
}


/*
	RightJoinOperatorExpression := ExplicitRightJoinOperatorExpression / NoJoinOperatorExpression
*/
shared_ptr <Expression> thel::parseRightJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExplicitRightJoinOperatorExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseNoJoinOperatorExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}
	
	a_eaten = 0;
	a_ok = false;
	return shared_ptr <Expression> {};
}


/*
	ExplicitRightJoinOperatorExpression := NoJoinOperatorExpression Space 
		RightJoinOperator Space 
		RightJoinOperatorExpression
*/
shared_ptr <ExpressionCompound> thel::parseExplicitRightJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;

	shared_ptr <Expression> left;

	{
		unsigned int eaten = 0;
		bool ok = false;
		left = parseNoJoinOperatorExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	string operator_name;

	{
		unsigned int eaten = 0;
		bool ok = false;
		operator_name = parseRightJoinOperator (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	shared_ptr <Expression> right;

	{
		unsigned int eaten = 0;
		bool ok = false;
		right = parseRightJoinOperatorExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	auto expression = make_shared <ExpressionCompound> ();
	
	expression->parameters = map <string, shared_ptr <Expression>> {
		{string {"action"}, make_shared <ExpressionString> (operator_name)},
		{string {"left"}, left},
		{string {"right"}, right},
	};

	a_eaten = accumulator;
	a_ok = true;
	return expression;
}


/*
	RightJoinOperator := "^and" / "^or"
*/
string thel::parseRightJoinOperator
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	vector <string> operator_codes = {
		string {"^and"},
		string {"^or"},
	};
	
	unsigned int eaten = 0;
	bool ok = false;
	string operator_code = parseLiterals (a_code, a_index, eaten, ok, operator_codes);
	
	if (! ok) {
		a_eaten = 0;
		a_ok = false;
		return string {};
	}
	
	map <string, string> operator_names = {
		{string {"^and"}, string {"and"}},
		{string {"^or"}, string {"or"}},
	};

	a_eaten = eaten;
	a_ok = true;

	return operator_names.at (operator_code);
}


/*
	NoJoinOperatorExpression := ExplicitNoJoinOperatorExpression / LeftJoinOperatorExpression
*/
shared_ptr <Expression> thel::parseNoJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExplicitNoJoinOperatorExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseLeftJoinOperatorExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}
	
	a_eaten = 0;
	a_ok = false;
	return shared_ptr <Expression> {};
}

/*
	ExplicitNoJoinOperatorExpression := LeftJoinOperatorExpression Space 
		NoJoinOperator Space 
		LeftJoinOperatorExpression
*/
shared_ptr <ExpressionCompound> thel::parseExplicitNoJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;
	
	shared_ptr <Expression> left;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		left = parseLeftJoinOperatorExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	string operator_name;

	{
		unsigned int eaten = 0;
		bool ok = false;
		operator_name = parseNoJoinOperator (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}
	
	shared_ptr <Expression> right;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		right = parseLeftJoinOperatorExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	a_eaten = accumulator;
	a_ok = true;
	
	auto expression = make_shared <ExpressionCompound> ();
	
	expression->parameters = map <string, shared_ptr <Expression>> {
		{string {"action"}, make_shared <ExpressionString> (operator_name)},
		{string {"left"}, left},
		{string {"right"}, right}
	};
	
	return expression;
}


/*
	NoJoinOperator := "=" / "<" / "^le" / "^is" / "^pair" / "^mod"
*/
string thel::parseNoJoinOperator
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	vector <string> operator_codes {
		string {"="},
		string {"<"},
		string {"^le"},
		string {"^is"},
		string {"^pair"},
		string {"^mod"},
	};
	
	unsigned int eaten = 0;
	bool ok = false;
	string operator_code = parseLiterals (a_code, a_index, eaten, ok, operator_codes);
	if (! ok) {
		a_eaten = 0;
		a_ok = false;
		return string {};
	}
	
	map <string, string> operator_names {
		{string {"="}, string {"equal"}},
		{string {"<"}, string {"less"}},
		{string {"^le"}, string {"lessorequal"}},
		{string {"^is"}, string {"is"}},
		{string {"^pair"}, string {"pair"}},
		{string {"^mod"}, string {"modulo"}},
	};
	
	a_eaten = eaten;
	a_ok = true;
	
	return operator_names.at (operator_code);
}


/*
	LeftJoinOperatorExpression := ExplicitLeftJoinOperatorExpression / UnaryOperatorExpression
*/
shared_ptr <Expression> thel::parseLeftJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExplicitLeftJoinOperatorExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseUnaryOperatorExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}
	
	a_eaten = 0;
	a_ok = false;
	return shared_ptr <Expression> {};
}


/*
	ExplicitLeftJoinOperatorExpression := UnaryOperatorExpression
		(Space LeftJoinOperator Space UnaryOperatorExpression)+
*/
shared_ptr <Expression> thel::parseExplicitLeftJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;
	
	shared_ptr <Expression> expression;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		expression = parseUnaryOperatorExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <Expression> {};
		}
		accumulator += eaten;
	}

	for (; ; ) {
		unsigned int accumulator_in_loop = 0;
	
		{
			unsigned int eaten = 0;
			bool ok = false;
			parseSpace (a_code, a_index + accumulator + accumulator_in_loop, eaten, ok);
			assert (ok);
			accumulator_in_loop += eaten;
		}

		string operator_name;

		{
			unsigned int eaten = 0;
			bool ok = false;
			operator_name = parseLeftJoinOperator
				(a_code, a_index + accumulator + accumulator_in_loop, eaten, ok);
			if (! ok) {
				break;
			}
			accumulator_in_loop += eaten;
		}

		{
			unsigned int eaten = 0;
			bool ok = false;
			parseSpace (a_code, a_index + accumulator + accumulator_in_loop, eaten, ok);
			assert (ok);
			accumulator_in_loop += eaten;
		}
		
		shared_ptr <Expression> right;
		
		{
			unsigned int eaten = 0;
			bool ok = false;
			right = parseUnaryOperatorExpression
				(a_code, a_index + accumulator + accumulator_in_loop, eaten, ok);
			if (! ok) {
				break;
			}
			accumulator_in_loop += eaten;
		}
		
		expression = make_shared <ExpressionCompound> (
			map <string, shared_ptr <Expression>> {
				{string {"action"}, make_shared <ExpressionString> (operator_name)},
				{string {"left"}, expression},
				{string {"right"}, right},
			}
		);
		
		accumulator += accumulator_in_loop;
	}

	a_eaten = accumulator;
	a_ok = true;
	return expression;
}


/*
	LeftJoinOperator := "+" / "-" / "*" / "/" / "," / "^as" / ">"
*/
string thel::parseLeftJoinOperator
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	vector <string> operator_codes {
		string {"+"},
		string {"-"},
		string {"*"},
		string {"/"},
		string {","},
		string {"^as"},
		string {">"},
	};
	
	unsigned int eaten = 0;
	bool ok = false;
	string operator_code = parseLiterals (a_code, a_index, eaten, ok, operator_codes);
	if (! ok) {
		a_eaten = 0;
		a_ok = false;
		return string {};
	}
	
	map <string, string> operator_names {
		{string {"+"}, string {"plus"}},
		{string {"-"}, string {"minus"}},
		{string {"*"}, string {"multiply"}},
		{string {"/"}, string {"divide"}},
		{string {","}, string {"append"}},
		{string {"^as"}, string {"as"}},
		{string {">"}, string {"andthen"}},
	};
	
	a_eaten = eaten;
	a_ok = true;
	
	return operator_names.at (operator_code);
}


/*
	UnaryOperatorExpression := ExplicitUnaryOperatorExpression / NonOperatorExpression
*/
shared_ptr <Expression> thel::parseUnaryOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExplicitUnaryOperatorExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseNonOperatorExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}
	
	a_eaten = 0;
	a_ok = false;
	return shared_ptr <Expression> {};
}


/*
	ExplicitUnaryOperatorExpression := UnaryOperator Space UnaryOperatorExpression
*/
shared_ptr <Expression> thel::parseExplicitUnaryOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;
	
	string operator_name;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		operator_name = parseUnaryOperator (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <Expression> {};
		}
		accumulator += eaten;
	}
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}
	
	shared_ptr <Expression> right;

	{
		unsigned int eaten = 0;
		bool ok = false;
		right = parseUnaryOperatorExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <Expression> {};
		}
		accumulator += eaten;
	}
	
	a_eaten = accumulator;
	a_ok = true;

	return make_shared <ExpressionCompound> (
		map <string, shared_ptr <Expression>> {
			{string {"action"}, make_shared <ExpressionString> (operator_name)},
			{string {"main"}, right},
		}
	);
}


/*
	UnaryOperator := "^not" / "#" / "^broken-pair"
*/
string thel::parseUnaryOperator
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	vector <string> operator_codes {
		string {"^not"},
		string {"#"},
		string {"^broken-pair"},
	};
	
	unsigned int eaten = 0;
	bool ok = false;
	string operator_code = parseLiterals (a_code, a_index, eaten, ok, operator_codes);
	if (! ok) {
		a_eaten = 0;
		a_ok = false;
		return string {};
	}
	
	map <string, string> operator_names {
		{string {"^not"}, string {"not"}},
		{string {"#"}, string {"stringtonumber"}},
		{string {"^broken-pair"}, string {"brokenpair"}},
	};
	
	a_eaten = eaten;
	a_ok = true;
	
	return operator_names.at (operator_code);
}


/*
NonOperatorExpression := LiteralExpression
	/ ExpressionInParentheses
	/ ParameterExpression
	/ SpecialExpression
	/ CompoundExpression
*/
shared_ptr <Expression> thel::parseNonOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	{
		unsigned int eaten = 0;
		bool ok = false;
		string string_value = parseString (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return make_shared <ExpressionString> (string_value);
		}
	}
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseExpressionWithParentheses (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseParameterExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseSpecialExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseCompoundExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	a_eaten = 0;
	a_ok = false;
	return shared_ptr <Expression> {};
}


/*
	ExpressionInParentheses := "(" Space Expression Space ")"
*/
shared_ptr <Expression> thel::parseExpressionWithParentheses
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {"("});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <Expression> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	shared_ptr <Expression> expression;

	{
		unsigned int eaten = 0;
		bool ok = false;
		expression = parseExpression (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <Expression> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {")"});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <Expression> {};
		}
		accumulator += eaten;
	}

	a_eaten = accumulator;
	a_ok = true;
	return expression;
}


/*
	ParameterExpression := ("$" Space)+ String
*/
shared_ptr <ExpressionParameter> thel::parseParameterExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {"$"});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionParameter> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	unsigned int depth = 0;

	for (; ; ) {
		unsigned int accululator_in_loop = 0;
		{
			unsigned int eaten = 0;
			bool ok = false;
			parseLiteral (a_code, a_index + accumulator + accululator_in_loop, eaten, ok, string {"$"});
			if (! ok) {
				break;
			}
			accululator_in_loop += eaten;
		}

		{
			unsigned int eaten = 0;
			bool ok = false;
			parseSpace (a_code, a_index + accumulator + accululator_in_loop, eaten, ok);
			assert (ok);
			accululator_in_loop += eaten;
		}
		
		depth ++;
		accumulator += accululator_in_loop;
	}

	string parameter_name;

	{
		unsigned int eaten = 0;
		bool ok = false;
		parameter_name = parseString (a_code, a_index + accumulator, eaten, ok);
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionParameter> {};
		}
		accumulator += eaten;
	}		

	a_eaten = accumulator;
	a_ok = true;
	
	auto expression = make_shared <ExpressionParameter> ();
	expression->depth = depth;
	expression->name = parameter_name;
	
	return expression;
}


/*
	SpecialExpression := "%" / "^true" / "^false"
*/
shared_ptr <ExpressionSpecial> thel::parseSpecialExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	vector <string> special_codes {
		string {"%"},
		string {"^true"},
		string {"^false"},
	};
	
	unsigned int eaten = 0;
	bool ok = false;
	string special_code = parseLiterals (a_code, a_index, eaten, ok, special_codes);
	if (! ok) {
		a_eaten = 0;
		a_ok = false;
		return shared_ptr <ExpressionSpecial> {};
	}
	
	a_eaten = eaten;
	a_ok = true;
	
	auto expression = make_shared <ExpressionSpecial> ();
	
	map <string, string> special_names {
		{string {"%"}, string {"latest"}},
		{string {"^true"}, string {"true"}},
		{string {"^false"}, string {"false"}},
	};
	
	expression->name = special_names.at (special_code);

	return expression;
}


/*
	CompoundExpression := NominalCompoundExpression
		/ EnumerativeCompoundExpression
*/
shared_ptr <ExpressionCompound> thel::parseCompoundExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseNominalCompoundExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		auto expression = parseEnumerativeCompoundExpression (a_code, a_index, eaten, ok);
		if (ok) {
			a_eaten = eaten;
			a_ok = true;
			return expression;
		}
	}

	a_eaten = 0;
	a_ok = false;
	return shared_ptr <ExpressionCompound> {};
}


/*
	NominalCompoundExpression := "[" Space (Parameter Space)+ "]"
*/
shared_ptr <ExpressionCompound> thel::parseNominalCompoundExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {"["});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	map <string, shared_ptr <Expression>> parameters;

	for (; ; ) {
		unsigned int accumulator_in_loop = 0;
		{
			unsigned int eaten = 0;
			bool ok = false;
			string parameter_name;
			shared_ptr <Expression> parameter_value;
			parseParameter (
				a_code,
				a_index + accumulator + accumulator_in_loop,
				eaten,
				ok,
				parameter_name,
				parameter_value
			);
			if (! ok) {
				break;
			}
			parameters.insert (pair <string, shared_ptr <Expression>> {
				parameter_name,
				parameter_value
			});
			accumulator_in_loop += eaten;
		}

		{
			unsigned int eaten = 0;
			bool ok = false;
			parseSpace (a_code, a_index + accumulator + accumulator_in_loop, eaten, ok);
			assert (ok);
			accumulator_in_loop += eaten;
		}
		
		accumulator += accumulator_in_loop;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {"]"});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	if (parameters.empty ()) {
		a_eaten = 0;
		a_ok = false;
		return shared_ptr <ExpressionCompound> {};
	}

	a_eaten = accumulator;
	a_ok = true;

	auto expression = make_shared <ExpressionCompound> (parameters);
	
	return expression;
}


/*
	EnumerativeCompoundExpression := "[" Space (Expression Space ";" Space)+ "]"
*/
shared_ptr <ExpressionCompound> thel::parseEnumerativeCompoundExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok)
{
	unsigned int accumulator = 0;
	
	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {"["});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseSpace (a_code, a_index + accumulator, eaten, ok);
		assert (ok);
		accumulator += eaten;
	}

	vector <shared_ptr <Expression>> parameter_values;

	for (; ; ) {
		unsigned int accumulator_in_loop = 0;
		shared_ptr <Expression> parameter_value;

		{
			unsigned int eaten = 0;
			bool ok = false;
			parameter_value = parseExpression
				(a_code, a_index + accumulator + accumulator_in_loop, eaten, ok);
			if (! ok) {
				break;
			}
			accumulator_in_loop += eaten;
		}

		{
			unsigned int eaten = 0;
			bool ok = false;
			parseSpace
				(a_code, a_index + accumulator + accumulator_in_loop, eaten, ok);
			assert (ok);
			accumulator_in_loop += eaten;
		}

		{
			unsigned int eaten = 0;
			bool ok = false;
			parseLiteral
				(a_code, a_index + accumulator + accumulator_in_loop, eaten, ok, string {";"});
			if (! ok) {
				break;
			}
			accumulator_in_loop += eaten;
		}

		{
			unsigned int eaten = 0;
			bool ok = false;
			parseSpace
				(a_code, a_index + accumulator + accumulator_in_loop, eaten, ok);
			assert (ok);
			accumulator_in_loop += eaten;
		}
		
		parameter_values.push_back (parameter_value);
		accumulator += accumulator_in_loop;
	}

	{
		unsigned int eaten = 0;
		bool ok = false;
		parseLiteral (a_code, a_index + accumulator, eaten, ok, string {"]"});
		if (! ok) {
			a_eaten = 0;
			a_ok = false;
			return shared_ptr <ExpressionCompound> {};
		}
		accumulator += eaten;
	}

	map <string, shared_ptr <Expression>> parameters;

	switch (parameter_values.size ()) {
	case 1:
		parameters = map <string, shared_ptr <Expression>> {
			{string {"action"}, parameter_values.at (0)},
		};
		break;
	case 2:
		parameters = map <string, shared_ptr <Expression>> {
			{string {"action"}, parameter_values.at (0)},
			{string {"main"}, parameter_values.at (1)},
		};
		break;
	case 3:
		parameters = map <string, shared_ptr <Expression>> {
			{string {"action"}, parameter_values.at (0)},
			{string {"method"}, parameter_values.at (1)},
			{string {"main"}, parameter_values.at (2)},
		};
		break;
	default:
		a_eaten = 0;
		a_ok = false;
		return shared_ptr <ExpressionCompound> {};
	}

	auto expression = make_shared <ExpressionCompound> (parameters);
	
	a_eaten = accumulator;
	a_ok = true;
	
	return expression;
}


