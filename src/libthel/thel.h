#ifndef THEL_H
#define THEL_H


#include <memory>
#include <map>
#include <string>
#include <vector>


namespace thel {


using namespace std;


extern string g_file;
extern string g_line;


class Node {
public:
	virtual string format () const {
		return string {};
	};
};


class NodeDirective: public Node {
public:
	string name;
	map <string, shared_ptr <class Expression>> parameters;
public:
	string format () const;
};


class Expression: public Node {
};


class ExpressionString: public Expression {
public:
	string value;
public:
	ExpressionString () { };
	ExpressionString (string a_value): value (a_value) {};
public:
	string format () const;
};


class ExpressionSpecial: public Expression {
public:
	string name;
public:
	string format () const;
};


class ExpressionParameter: public Expression {
public:
	unsigned int depth;
	string name;
public:
	string format () const;
};


class ExpressionCompound: public Expression {
public:
	map <string, shared_ptr <class Expression>> parameters;
public:
	ExpressionCompound () { };
	ExpressionCompound
		(map <string, shared_ptr <class Expression>> a_parameters):
		parameters (a_parameters)
		{ };
public:
	string format () const;
};


class CacheInput {
public:
	string code;
	unsigned int index;
public:
	bool operator < (const CacheInput &right) const {
		return code < right.code
			|| (code == right.code && index < right.index);
	};
};


class CacheOutput {
public:
	unsigned int eaten;
	bool ok;
	shared_ptr <Expression> expression;
};


extern map <CacheInput, CacheOutput> g_cache;


string parse (string in);
string parse (string in, string placement);


shared_ptr <NodeDirective> parseDirective
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <NodeDirective> parseFullDirective
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
string parseDirectiveName
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
string parseLiterals
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok, vector <string> a_literals);
void parseSpace
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
void parseLiteral
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok, string a_literal);
void parseParameter (
	string a_code,
	unsigned int a_index,
	unsigned int &a_eaten,
	bool &a_ok,
	string &a_parameter_name,
	shared_ptr <Expression> &a_parameter_value
);
shared_ptr <NodeDirective> parseEasyDirective
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
string parseString
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
string parseNominalString
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
string parseLiteralString
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
string parseSpecialString
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);

shared_ptr <Expression> parseExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <Expression> parseExpressionWithCache
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <Expression> parseExpressionImpl
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);

shared_ptr <Expression> parseThenElseOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <ExpressionCompound> parseExplicitThenElseOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <Expression> parseRightJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <ExpressionCompound> parseExplicitRightJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
string parseRightJoinOperator
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <Expression> parseNoJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <ExpressionCompound> parseExplicitNoJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
string parseNoJoinOperator
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <Expression> parseLeftJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <Expression> parseExplicitLeftJoinOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
string parseLeftJoinOperator
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <Expression> parseUnaryOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <Expression> parseExplicitUnaryOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
string parseUnaryOperator
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <Expression> parseNonOperatorExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <Expression> parseExpressionWithParentheses
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <ExpressionParameter> parseParameterExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <ExpressionSpecial> parseSpecialExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <ExpressionCompound> parseCompoundExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <ExpressionCompound> parseNominalCompoundExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);
shared_ptr <ExpressionCompound> parseEnumerativeCompoundExpression
	(string a_code, unsigned int a_index, unsigned int &a_eaten, bool &a_ok);


void test ();


}; /* namespace thel { */


#endif /* #ifndef THEL_H */

