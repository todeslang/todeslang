#include <vector>
#include <sstream>

#include "thel.h"


using namespace thel;
using namespace std;


static string escape_json (string in)
{
	string out;
	for (auto c: in) {
		if (c == '\n') {
			out += string {"\\n"};
		} else if (c == '\r') {
			out += string {"\\r"};
		} else if (c == '\t') {
			out += string {"\\t"};
		} else if (c == '"') {
			out += string {"\\\""};
		} else if (c == '\\') {
			out += string {"\\\\"};
		} else if (0x00 <= c && c < 0x20) {
			out += string {"�"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


string NodeDirective::format () const
{
	string out;
	
	out +=
		string {"{"}
		+ string {"\"class\":\"directive\","}
		+ string {"\"name\":\""} + escape_json (name) + string {"\","};
	
	vector <string> parameter_codes;
	
	for (auto parameter: parameters) {
		string parameter_name = parameter.first;
		shared_ptr <Expression> parameter_value = parameter.second;

		string parameter_code;
		parameter_code
			+= string {"{"};
		parameter_code
			+= string {"\"name\":\""} + escape_json (parameter_name) + string {"\","};
		parameter_code
			+= string {"\"value\":"} + parameter_value->format ();
		parameter_code
			+= string {"}"};
		parameter_codes.push_back (parameter_code);
	}

	out +=
		string {"\"parameters\":["};

	for (unsigned int cn = 0; cn < parameter_codes.size (); cn ++) {
		if (0 < cn) {
			out += string {","};
		}
		out += parameter_codes.at (cn);
	}
	
	out +=
		string {"]"};
	
	out +=
		string {"}"};
	
	return out;
}


string ExpressionString::format () const
{
	return
		string {"{"}
		+ string {"\"class\":\"expressionString\","}
		+ string {"\"value\":\""} + escape_json (value) + string {"\""}
		+ string {"}"};
}


string ExpressionSpecial::format () const
{
	return
		string {"{"}
		+ string {"\"class\":\"expressionSpecial\","}
		+ string {"\"name\":\""} + escape_json (name) + string {"\""}
		+ string {"}"};
}


string ExpressionParameter::format () const
{
	stringstream stream;
	stream << depth;

	return
		string {"{"}
		+ string {"\"class\":\"expressionParameter\","}
		+ string {"\"depth\":"} + stream.str () + string {","}
		+ string {"\"name\":\""} + escape_json (name) + string {"\""}
		+ string {"}"};
}


string ExpressionCompound::format () const
{
	string out;
	
	out +=
		string {"{"}
		+ string {"\"class\":\"expressionCompound\","};
	
	vector <string> parameter_codes;
	
	for (auto parameter: parameters) {
		string parameter_name = parameter.first;
		shared_ptr <Expression> parameter_value = parameter.second;

		string parameter_code;
		parameter_code
			+= string {"{"};
		parameter_code
			+= string {"\"name\":\""} + escape_json (parameter_name) + string {"\","};
		parameter_code
			+= string {"\"value\":"} + parameter_value->format ();
		parameter_code
			+= string {"}"};
		parameter_codes.push_back (parameter_code);
	}

	out +=
		string {"\"parameters\":["};

	for (unsigned int cn = 0; cn < parameter_codes.size (); cn ++) {
		if (0 < cn) {
			out += string {","};
		}
		out += parameter_codes.at (cn);
	}
	
	out +=
		string {"]"};
	
	out +=
		string {"}"};
	
	return out;
}

