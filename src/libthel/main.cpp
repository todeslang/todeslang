#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <cassert>
#include <sstream>

#include "thel.h"


using namespace std;


int main (int argc, char *argv [])
{
	if (1 < argc) {
		cout << "thel < input.txt > output.cjson" << endl;
		exit (0);
	}

	string in_string;

	for (; ; ) {
		if (cin.eof ()) {
			break;
		}
		string buffer;
		getline (cin, buffer);
		in_string += buffer += string {"\n"};
		
	}

	cout << thel::parse (in_string);
	return 0;
}


