#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <cassert>
#include <sstream>

#include "thel.h"


using namespace thel;
using namespace std;


string thel::g_file;
string thel::g_line;
map <CacheInput, CacheOutput> thel::g_cache;


static bool is_white_space_line (string in)
{
	for (auto c: in) {
		if (! (c == ' ' || c == '\t')) {
			return false;
		}
	}
	return true;
}


static string parse_directive (string in, unsigned int line_number)
{
	stringstream line_stream;
	line_stream << hex << (line_number);
	g_line = line_stream.str ();

	unsigned int index = 0;
	unsigned int eaten = 0;
	bool ok = false;
	auto node_directive = parseDirective (in, index, eaten, ok);

	if (ok && eaten == in.size ()) {
		return node_directive->format ();
	}
	return string {};
}


static vector <string> split (string in) {
	vector <string> out;
	string buffer;
	for (auto c: in) {
		if (c == '\n') {
			out.push_back (buffer);
			buffer.clear ();
		} else {
			buffer.push_back (c);
		}
	}
	out.push_back (buffer);
	return out;
}


string thel::parse (string in_string)
{
	return parse (in_string, string {"anywhare"});
}


string thel::parse (string in_string, string placement)
{
	g_file = placement;

	string super_out;

	auto in_vector = split (in_string);

	unsigned int state = 0;
	string directive_code;
	unsigned int directive_line_number = 0;

	for (unsigned int line_number = 0; line_number < in_vector.size (); line_number ++) {
		string buffer = in_vector.at (line_number);

		switch (state) {
		case 0:
			if (is_white_space_line (buffer)) {
				if (! directive_code.empty ()) {
					string out = parse_directive (directive_code, directive_line_number + 1);
					if (out.empty ()) {
						cerr
							<< "Syntax error after the line "
							<< directive_line_number + 1 << " in: "
							<< placement
							<< endl;
					} else {
						super_out += out + string {"\n"};
					}
				}
				directive_code.clear ();
				state = 1;
			} else {
				directive_code += buffer;
			}
			break;
		case 1:
			if (is_white_space_line (buffer)) {
				/* Do nothing. */
			} else {
				directive_code += buffer;
				directive_line_number = line_number;
				state = 0;
			}
			break;
		default:
			assert (false);
			break;
		}
	}

	if (! directive_code.empty ()) {
		string out = parse_directive (directive_code, directive_line_number);
		if (out.empty ()) {
			cerr
				<< "Syntax error after the line "
				<< directive_line_number << " in: "
				<< placement
				<< endl;
		} else {
				super_out += out + string {"\n"};
		}
	}

	return super_out;
}


